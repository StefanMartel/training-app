# Modules

## Modules in workspace

| Module | Declarations | Imports | Exports | Bootstrap | Providers | Entry points |
| ---| --- | --- | --- | --- | --- | --- |
| AppRoutingModule | 0 | 1 | 1 | 0 | 0 | 0 |
| AppModule | 1 | 9 | 0 | 1 | 11 | 0 |
| ExercisePageModule | 2 | 2 | 0 | 0 | 1 | 0 |
| MapPageModule | 1 | 2 | 0 | 0 | 0 | 0 |
| MaterialModule | 0 | 15 | 15 | 0 | 2 | 0 |
| PasswordModule | 3 | 3 | 0 | 0 | 0 | 0 |
| ProgressionPageModule | 1 | 2 | 0 | 0 | 0 | 0 |
| SharedModule | 4 | 8 | 10 | 0 | 0 | 0 |
| TrainingModule | 4 | 2 | 0 | 0 | 3 | 0 |
| UserModule | 3 | 3 | 0 | 0 | 0 | 0 |

## AppRoutingModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\app-routing.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations |  |
| Imports | RouterModule.forRoot(routes) |
| Exports | RouterModule |
| Bootstrap |  |
| Providers |  |
| Entry components |  |

## AppModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\app.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | AppComponent |
| Imports | AppRoutingModule,<br>BrowserAnimationsModule,<br>BrowserModule,<br>EffectsModule.forRoot([TrainingEffects ExerciseEffects UserEffects]),<br>IonicModule.forRoot(),<br>SharedModule,<br>StoreDevtoolsModule.instrument({maxAge: 10}),<br>StoreModule.forRoot({ reducers }),<br>TranslateModule.forRoot({loader: {provide: TranslateLoaderuseFactory: createTranslateLoaderdeps: [HttpClient]}}) |
| Exports |  |
| Bootstrap | AppComponent |
| Providers | ExerciseService,<br>LoadingService,<br>SplashScreen,<br>StatusBar,<br>StorageService,<br>TrainingService,<br>UrlConfiguration,<br>UserService,<br>{ provide: HttpRepo useFactory: httpRepo deps: [HttpClient] },<br>{ provide: HTTP_INTERCEPTORS useClass: ErrorInterceptor multi: true },<br>{ provide: RouteReuseStrategy useClass: IonicRouteStrategy } |
| Entry components |  |

## ExercisePageModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\features\exercise\exercise.page.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | ExerciseListPage,<br>ExercisePage |
| Imports | RouterModule.forChild([{ path: '' pathMatch: 'full' component: ExerciseListPage }{ path: ':exerciseId' pathMatch: 'full' component: ExercisePage }]),<br>SharedModule |
| Exports |  |
| Bootstrap |  |
| Providers | ExerciseService |
| Entry components |  |

## MapPageModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\features\map\map.page.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | MapPage |
| Imports | RouterModule.forChild([{path: ''component: MapPage}]),<br>SharedModule |
| Exports |  |
| Bootstrap |  |
| Providers |  |
| Entry components |  |

## MaterialModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\shared\material.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations |  |
| Imports | DragDropModule,<br>MatButtonModule,<br>MatCardModule,<br>MatDatepickerModule,<br>MatDialogModule,<br>MatExpansionModule,<br>MatIconModule,<br>MatInputModule,<br>MatListModule,<br>MatNativeDateModule,<br>MatProgressSpinnerModule,<br>MatRippleModule,<br>MatSelectModule,<br>MatSnackBarModule,<br>NgxMaterialTimepickerModule |
| Exports | DragDropModule,<br>MatButtonModule,<br>MatCardModule,<br>MatDatepickerModule,<br>MatDialogModule,<br>MatExpansionModule,<br>MatIconModule,<br>MatInputModule,<br>MatListModule,<br>MatNativeDateModule,<br>MatProgressSpinnerModule,<br>MatRippleModule,<br>MatSelectModule,<br>MatSnackBarModule,<br>NgxMaterialTimepickerModule |
| Bootstrap |  |
| Providers | MatDatepickerModule,<br>{provide: MAT_DATE_LOCALE useValue: 'fr-FR'} |
| Entry components |  |

## PasswordModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\features\user\pages\password\password.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | CodeVerificationPage,<br>LostPasswordPage,<br>NewPasswordPage |
| Imports | CommonModule,<br>RouterModule.forChild(routes),<br>SharedModule |
| Exports |  |
| Bootstrap |  |
| Providers |  |
| Entry components |  |

## ProgressionPageModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\features\progression\progression.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | ProgressionPage |
| Imports | RouterModule.forChild([{ path: '' pathMatch: 'full' component: ProgressionPage canActivate: [IsSignedInGuard] }]),<br>SharedModule |
| Exports |  |
| Bootstrap |  |
| Providers |  |
| Entry components |  |

## SharedModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\shared\shared.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | ConfirmActionComponent,<br>ListItemComponent,<br>LoadingComponent,<br>SideMenuComponent |
| Imports | CommonModule,<br>FormsModule,<br>HighchartsChartModule,<br>HttpClientModule,<br>IonicModule,<br>MaterialModule,<br>ReactiveFormsModule,<br>TranslateModule |
| Exports | CommonModule,<br>FormsModule,<br>HighchartsChartModule,<br>IonicModule,<br>ListItemComponent,<br>LoadingComponent,<br>MaterialModule,<br>ReactiveFormsModule,<br>SideMenuComponent,<br>TranslateModule |
| Bootstrap |  |
| Providers |  |
| Entry components |  |

## TrainingModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\features\training\training.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | RepComponent,<br>TrainingExerciseComponent,<br>TrainingListPage,<br>TrainingPage |
| Imports | RouterModule.forChild([{ path: '' pathMatch: 'full' component: TrainingListPage canActivate: [IsSignedInGuard] }{ path: ':trainingId' pathMatch: 'full' component: TrainingPage resolve: { training: TrainingByIdResolver } }]),<br>SharedModule |
| Exports |  |
| Bootstrap |  |
| Providers | TrainingByIdResolver,<br>TrainingResolver,<br>TrainingService |
| Entry components |  |

## UserModule

Filename: c:\Users\Stefan\Desktop\Projet\Angular\training\src\app\features\user\user.module.ts

| Section | Classes, service, modules |
| ---- |:-----------|
| Declarations | LoginPage,<br>LogoutPage,<br>SubscribePage |
| Imports | CommonModule,<br>RouterModule.forChild(routes),<br>SharedModule |
| Exports |  |
| Bootstrap |  |
| Providers |  |
| Entry components |  |

