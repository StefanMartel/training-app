import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.nataisConsulting.TrainingList',
  appName: 'Trainarizor',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
