import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'trainings', pathMatch: 'full' },
  {
    path: 'user',
    loadChildren: () => import('./features/user/user.module').then(m => m.UserModule),
  },
  {
    path: 'trainings',
    loadChildren: () => import('./features/training/training.module').then(m => m.TrainingModule),
  },
  {
    path: 'exercises',
    loadChildren: () => import('./features/exercise/exercise.page.module').then(m => m.ExercisePageModule),
  },
  { path: 'map', loadChildren: () => import('./features/map/map.page.module').then(m => m.MapPageModule) },
  {
    path: 'progression',
    loadChildren: () => import('./features/progression/progression.module').then( m => m.ProgressionPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
