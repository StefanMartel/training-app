import { Injectable } from '@angular/core';

export enum urlGroupEnum {
  'TRAINING' = 'api/training',
  'EXERCISE' = 'api/exercise',
  'USER' = 'api/user',
}

export interface BackEndUrlList {
  connectUser: string;
  disconnectUser: string;
  resetPassword: string;
  validateCode: string;
  changePassword: string;
  getTrainingListByUser: string;
  addTraining: string;
  duplicateTraining: string;
  updateTraining: string;
  addExerciseToTraining: string;
  updateExerciseTraining: string;
  deleteExerciseToTraining: string;
  deleteTrainingByTrainingId: string;
  getExerciseList: string;
  addExercise: string;
  updateExercise: string;
  deleteExerciseByExerciseId: string;
}

@Injectable()
export class UrlConfiguration {
  public backEndUrlList: BackEndUrlList = {
    connectUser: `${urlGroupEnum.USER}/{user}/connect/{password}`,
    disconnectUser: `${urlGroupEnum.USER}/{user}/disconnect`,
    resetPassword: `${urlGroupEnum.USER}/{email}/password/reset`,
    validateCode: `${urlGroupEnum.USER}/{user}/code/validate/{code}`,
    changePassword: `${urlGroupEnum.USER}/{user}/password/change/{password}`,
    getTrainingListByUser: `${urlGroupEnum.TRAINING}/{user}/list`,
    addTraining: `${urlGroupEnum.TRAINING}/add`,
    duplicateTraining: `${urlGroupEnum.TRAINING}/duplicate`,
    updateTraining: `${urlGroupEnum.TRAINING}/update`,
    addExerciseToTraining: `${urlGroupEnum.TRAINING}/{trainingId}/exercise/add`,
    updateExerciseTraining: `${urlGroupEnum.TRAINING}/{trainingId}/exercise/update`,
    deleteExerciseToTraining: `${urlGroupEnum.TRAINING}/{trainingId}/exercise/{exerciseId}/delete`,
    deleteTrainingByTrainingId: `${urlGroupEnum.TRAINING}/{trainingId}/delete`,
    getExerciseList: `${urlGroupEnum.EXERCISE}/list`,
    addExercise: `${urlGroupEnum.EXERCISE}/add`,
    updateExercise: `${urlGroupEnum.EXERCISE}/update`,
    deleteExerciseByExerciseId: `${urlGroupEnum.EXERCISE}/{exerciseId}/delete`,
  };

  /**
   * renvoie l'URL devant être utilisée pour les appels aux backEnd
   * @param  key Clef désignant l'URI à utiliser (PNC, career-objective, etc...)
   * @param  params paramètres à intégrer dans l'URL (tableau de param)
   * @return url du backEnd vers laquelle pointer
   */
  public getBackEndUrl(key: keyof BackEndUrlList, params: (string | number)[] = []): string {
    let urlToModify: string = this.backEndUrlList[key];
    // Les paramètres étant entre crochets, il faut les identifier avant de les remplacer
    // Les noms ne sont jamais les mêmes
    const regexToReplace = /{[a-zA-Z]*}/;
    // Remplacement des paramètres de l'URL
    for (let i = 0; i < params.length; i++) {
      urlToModify = urlToModify.replace(regexToReplace, params[i].toString());
    }
    return urlToModify;
  }
}
