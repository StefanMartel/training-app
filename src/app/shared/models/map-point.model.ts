export interface MapPointModel {
  latitude: number;
  longitude: number;
  text?: string;
  color?: string;
}
