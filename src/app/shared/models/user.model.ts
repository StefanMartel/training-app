export class UserModel {

  constructor(
    readonly login: string,
    readonly email?: string,
    readonly firstName?: string,
    readonly lastName?: string
  ) {}
}
