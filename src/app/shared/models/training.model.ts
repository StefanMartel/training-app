export interface TrainingModel {
  user?: string;
  id: string;
  title: string;
  creationDate: Date;
  weigth?: number;
  pct_muscle?: number;
  pct_fat?: number;
  exercises?: TrainingExercise[];
}

export interface TrainingExercise {
  exerciseId: string;
  exerciseOrder: number;
  exerciseLabel?: string;
  exerciseSelected: boolean;
  reps?: TrainingExerciseRep[];
}

export interface TrainingExerciseRep {
  time?: number;
  reps?: number;
  weight?: number;
  distance?: number;
  kcal?: number;
  selected: boolean;
}

export interface TrainingsModelResolver {
  data: TrainingModel[];
  error?: string;
}

export interface TrainingModelResolver {
  data?: TrainingModel;
  error?: string;
}
