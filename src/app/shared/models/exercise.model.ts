export interface ExerciseModel {
  id: string;
  title: string;
  videoLink: string;
  imgLink: string;
  creationDate: Date;
}
