import { MetaReducer, combineReducers } from '@ngrx/store';
import { userReducer } from '../user/user.reducer';
import { exerciseReducer } from '../exercise/exercise.reducer';
import { trainingReducer } from '../training/training.reducer';

export interface IAppState {
  training: any;
  exercise: any;
  user: any;
}

export const reducers = combineReducers({
  // @ts-ignore
  training: trainingReducer,
  // @ts-ignore
  exercise: exerciseReducer,
  // @ts-ignore
  user: userReducer,
});

export const metaReducers: MetaReducer<IAppState>[] = [];
