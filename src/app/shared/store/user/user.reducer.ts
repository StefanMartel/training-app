import {UserActions, UserActionTypes} from './user.action';
import {initialState} from './user.state';

export const userReducer = (state = initialState, action: UserActions) => {
  switch (action.type) {
    case UserActionTypes.SHOW_USER:
      return {...state};
    case UserActionTypes.ADD_USER:
    case UserActionTypes.CONNECT_USER:
    case UserActionTypes.DISCONNECT_USER:
    case UserActionTypes.CHANGE_PASSWORD_USER:
    case UserActionTypes.RESET_PASSWORD_USER:
    case UserActionTypes.VALIDATE_CODE_USER:
      return {...state, loading: true};
    case UserActionTypes.ADD_USER_COMPLETED:
    case UserActionTypes.CONNECT_USER_COMPLETED:
    case UserActionTypes.SET_USER:
      return {...state, loading: false, user: action.user};
    case UserActionTypes.DISCONNECT_USER_COMPLETED:
      return {...state, loading: false, user: null};
    case UserActionTypes.CHANGE_PASSWORD_USER_COMPLETED:
    case UserActionTypes.RESET_PASSWORD_USER_COMPLETED:
    case UserActionTypes.VALIDATE_CODE_USER_COMPLETED:
      return {...state, loading: false};
    default:
      return state;
  }
};
