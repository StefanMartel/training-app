import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';

import { InfoDisplayService } from '../../services/info-display.service';
import {UserService} from '../../services/user.service';
import {
  ChangePasswordAction, ChangePasswordCompletedAction,
  ConnectUserAction,
  ConnectUserCompletedAction,
  DisconnectUserAction,
  DisconnectUserCompletedAction,
  ErrorAction, ResetPasswordAction, ResetPasswordCompletedAction,
  UserActionTypes, ValidateActionCompletedAction, ValidateCodeAction,
} from './user.action';
import {UserModel} from '../../models/user.model';
import {typeOfMessageToDisplay} from '../../models/enums/message-to-display.model';
import {ErrorResponse} from '../../models/error-response.model';
import {StorageService, StorageType} from '../../services/storage.service';
import {GetTrainingListAction} from '../training/training.action';
import {Store} from '@ngrx/store';
import {ReturnResponseModel} from '../../models/return-response.model';
import { returnCode } from "../../util";
import {GetExerciseListAction} from "../exercise/exercise.action";

@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private userService: UserService,
    private infoDisplayService: InfoDisplayService,
    private translate: TranslateService,
    private router: Router,
    private store: Store
  ) {}

  connectUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActionTypes.CONNECT_USER),
      switchMap((option: ConnectUserAction) => {
        return this.userService.connectUserBack(option.login, option.password).pipe(
          map((user: UserModel | ErrorResponse) => {
              if(user.hasOwnProperty('login')){
                  user = user as UserModel;
                  this.infoDisplayService.displaySnackBarMessage(
                      this.translate.instant('CONFIRM.ACTION.CONNECTION', {firstName: user.firstName}),
                      typeOfMessageToDisplay.info,
                  );
                  StorageService.setObject(StorageType.USER, user).then((
                      value => console.log('TEH VALPUE', value)
                  )).catch(
                      error => console.log('ERROR VALUE', error)
                  )
                  this.store.dispatch(new GetTrainingListAction(user.login));
                  this.store.dispatch(new GetExerciseListAction());
                  this.router.navigate(['trainings'])
                  return new ConnectUserCompletedAction(user);
              } else {
                  this.infoDisplayService.displaySnackBarMessage(
                  this.translate.instant('CONFIRM.ACTION.CONNECTION_ERROR'),
                  typeOfMessageToDisplay.error,
                );
                return new ErrorAction();
              }
          }),
          catchError(() => {
              this.infoDisplayService.displaySnackBarMessage(
                  this.translate.instant('CONFIRM.ACTION.CONNECTION_ERROR'),
                  typeOfMessageToDisplay.error,
              );
              return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

    disconnectUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActionTypes.DISCONNECT_USER),
            switchMap((option: DisconnectUserAction) => {
                return this.userService.disconnectUserBack(option.login).pipe(
                    map((response: ReturnResponseModel | ErrorResponse) => {
                        if(response.hasOwnProperty('code')){
                            this.infoDisplayService.displaySnackBarMessage(
                                this.translate.instant('CONFIRM.ACTION.DISCONNECTION'),
                                typeOfMessageToDisplay.info,
                            );
                            StorageService.setObject(StorageType.USER, null);
                            this.router.navigate([''])
                            return new DisconnectUserCompletedAction();
                        } else {
                            this.infoDisplayService.displaySnackBarMessage(
                                this.translate.instant('CONFIRM.ACTION.DISCONNECTION_ERROR'),
                                typeOfMessageToDisplay.error,
                            );
                            return new ErrorAction();
                        }
                    }),
                    catchError(() => {
                        this.infoDisplayService.displaySnackBarMessage(
                            this.translate.instant('CONFIRM.ACTION.DISCONNECTION_ERROR'),
                            typeOfMessageToDisplay.error,
                        );
                        return of(new ErrorAction());
                    }),
                );
            }),
        ),
    );

  resetUserPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActionTypes.RESET_PASSWORD_USER),
      switchMap((option: ResetPasswordAction) => {
        return this.userService.resetPasswordBack(option.email).pipe(
          map((response: ReturnResponseModel | ErrorResponse) => {
            if(response.hasOwnProperty('code')){
              this.infoDisplayService.displaySnackBarMessage(
                this.translate.instant('CONFIRM.ACTION.RESET_PASSWORD'),
                typeOfMessageToDisplay.info,
              );
              this.router.navigate(['/user/password/code-verification'])
              return new ResetPasswordCompletedAction();
            } else {
              this.infoDisplayService.displaySnackBarMessage(
                this.translate.instant('CONFIRM.ACTION.RESET_PASSWORD_ERROR'),
                typeOfMessageToDisplay.error,
              );
              return new ErrorAction();
            }
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.RESET_PASSWORD_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  validateCodeUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActionTypes.VALIDATE_CODE_USER),
      switchMap((option: ValidateCodeAction) => {
        return this.userService.validateCodeBack(option.code).pipe(
          map((response: ReturnResponseModel | ErrorResponse) => {
            if(response.hasOwnProperty('code')){
              this.router.navigate(['/user/password/change'])
              return new ValidateActionCompletedAction();
            } else {
              const responseError = response as ErrorResponse;
              if(responseError.errorCode === returnCode.notGoodVerificationCode.code){
                this.infoDisplayService.displaySnackBarMessage(
                  this.translate.instant('CONFIRM.ACTION.VALIDATE_CODE_ERROR'),
                  typeOfMessageToDisplay.error,
                );
              }
              if(responseError.errorCode === returnCode.noMoreVerificationCodeAttempt.code) {
                this.infoDisplayService.displaySnackBarMessage(
                  this.translate.instant('CONFIRM.ACTION.NO_MORE_CODE_ATTEMPT'),
                  typeOfMessageToDisplay.error,
                );
                this.router.navigate(['/login'])
              }
              return new ErrorAction();
            }
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.VALIDATE_CODE_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  changeUserPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActionTypes.CHANGE_PASSWORD_USER),
      switchMap((option: ChangePasswordAction) => {
        return this.userService.changePasswordBack(option.password).pipe(
          map((response: ReturnResponseModel | ErrorResponse) => {
            if(response.hasOwnProperty('code')){
              this.infoDisplayService.displaySnackBarMessage(
                this.translate.instant('CONFIRM.ACTION.CHANGE_PASSWORD'),
                typeOfMessageToDisplay.info,
              );
              this.router.navigate(['/user'])
              return new ChangePasswordCompletedAction();
            } else {
              this.infoDisplayService.displaySnackBarMessage(
                this.translate.instant('CONFIRM.ACTION.CHANGE_PASSWORD_ERROR'),
                typeOfMessageToDisplay.error,
              );
              return new ErrorAction();
            }
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.RESET_PASSWORD_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );
}
