import { Action } from '@ngrx/store';
import {UserModel} from '../../models/user.model';

export enum UserActionTypes {
  SHOW_USER = 'User show Request',
  SET_USER = 'Set user in the store',
  ADD_USER = 'User add request',
  ADD_USER_COMPLETED = 'User add request completed',
  CONNECT_USER = 'Connect user request',
  CONNECT_USER_COMPLETED = 'Connect user request completed',
  DISCONNECT_USER = 'Deconnect user request',
  DISCONNECT_USER_COMPLETED = 'Deconnect user request completed',
  RESET_PASSWORD_USER = 'Reset user password request',
  RESET_PASSWORD_USER_COMPLETED = 'Reset user password request completed',
  VALIDATE_CODE_USER = 'code validation request',
  VALIDATE_CODE_USER_COMPLETED = 'code validation request completed',
  CHANGE_PASSWORD_USER = 'Change user password request',
  CHANGE_PASSWORD_USER_COMPLETED = 'Change user password request completed',
  ERROR_ACTION = 'Action to launch when an error occured',
  NO_ACTION = 'Aucune action de faite (suite à erreur dans l/effect)',
}

export class ShowUserAction implements Action {
  readonly type = UserActionTypes.SHOW_USER;
}

export class SetUserAction implements Action {
  readonly type = UserActionTypes.SET_USER;
  constructor(public user: UserModel) {}
}

export class AddUserAction implements Action {
  readonly type = UserActionTypes.ADD_USER;
  constructor(public user: UserModel, public password: string) {}
}

export class AddUserCompletedAction implements Action {
  readonly type = UserActionTypes.ADD_USER_COMPLETED;
  constructor(public user: UserModel) {}
}

export class ConnectUserAction implements Action {
  readonly type = UserActionTypes.CONNECT_USER;
  constructor(public login: string, public password: string) {}
}

export class ConnectUserCompletedAction implements Action {
  readonly type = UserActionTypes.CONNECT_USER_COMPLETED;
  constructor(public user: UserModel) {}
}

export class DisconnectUserAction implements Action {
  readonly type = UserActionTypes.DISCONNECT_USER;
  constructor(public login: string) {}
}

export class DisconnectUserCompletedAction implements Action {
  readonly type = UserActionTypes.DISCONNECT_USER_COMPLETED;
}

export class ResetPasswordAction implements Action {
  readonly type = UserActionTypes.RESET_PASSWORD_USER;
  constructor(public email: string) {}
}

export class ResetPasswordCompletedAction implements Action {
  readonly type = UserActionTypes.RESET_PASSWORD_USER_COMPLETED;
}

export class ValidateCodeAction implements Action {
  readonly type = UserActionTypes.VALIDATE_CODE_USER;
  constructor(public code: string) {}
}

export class ValidateActionCompletedAction implements Action {
  readonly type = UserActionTypes.VALIDATE_CODE_USER_COMPLETED;
}

export class ChangePasswordAction implements Action {
  readonly type = UserActionTypes.CHANGE_PASSWORD_USER;
  constructor(public password: string) {}
}

export class ChangePasswordCompletedAction implements Action {
  readonly type = UserActionTypes.CHANGE_PASSWORD_USER_COMPLETED;
}

export class NoAction implements Action {
  readonly type = UserActionTypes.NO_ACTION;
}

export class ErrorAction implements Action {
  readonly type = UserActionTypes.ERROR_ACTION;
}

export type UserActions =
  ShowUserAction
  | SetUserAction
  | AddUserAction
  | AddUserCompletedAction
  | ConnectUserAction
  | ConnectUserCompletedAction
  | DisconnectUserAction
  | DisconnectUserCompletedAction
  | ResetPasswordAction
  | ResetPasswordCompletedAction
  | ValidateCodeAction
  | ValidateActionCompletedAction
  | ChangePasswordAction
  | ChangePasswordCompletedAction
  | ErrorAction
  | NoAction;
