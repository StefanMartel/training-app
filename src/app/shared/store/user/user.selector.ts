import { createSelector } from '@ngrx/store';

export const selectUserState = (state: any) => state.reducers.user;

export const selectUser = createSelector(selectUserState, user => user.user);

export const isUserLoading = createSelector(selectUserState, user => {
    return user.loading;
});
