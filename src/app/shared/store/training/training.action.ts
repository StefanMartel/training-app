import { Action } from '@ngrx/store';
import { TrainingExercise, TrainingModel } from '../../models/training.model';

export enum TrainingListActionTypes {
  GET_TRAINING_LIST = 'Training list get Request',
  GET_TRAINING_LIST_COMPLETED = 'Training list get completed',
  ADD_TRAINING = 'Training list add Request',
  ADD_TRAINING_COMPLETED = 'Training list add completed',
  DUPLICATE_TRAINING = 'Training list duplicate Request',
  DUPLICATE_TRAINING_COMPLETED = 'Training list duplicate completed',
  UPDATE_TRAINING = 'Training list update Request',
  UPDATE_TRAINING_COMPLETED = 'Training list update completed',
  DELETE_TRAINING = 'Training delete Request',
  DELETE_TRAINING_COMPLETED = 'Training delete completed',
  ADD_EXERCISE_TO_TRAINING = 'Add an exercise to defined training',
  ADD_EXERCISE_TO_TRAINING_COMPLETED = 'Add an exercise to defined training completed',
  UPDATE_EXERCISE_TO_TRAINING = 'Update an exercise to defined training',
  UPDATE_EXERCISE_TO_TRAINING_COMPLETED = 'Update an exercise to defined training completed',
  DELETE_EXERCISE_TO_TRAINING = 'Delete an exercise to defined training',
  DELETE_EXERCISE_TO_TRAINING_COMPLETED = 'Deleted an exercise to defined training completed',
  ERROR_ACTION = 'Action to launch when an error occured',
  NO_ACTION = 'Aucune action de faite (suite à erreur dans l/effect)',
}

export class GetTrainingListAction implements Action {
  readonly type = TrainingListActionTypes.GET_TRAINING_LIST;
  constructor(public userLogin: string) {}
}

export class GetTrainingListCompletedAction implements Action {
  readonly type = TrainingListActionTypes.GET_TRAINING_LIST_COMPLETED;
  constructor(public trainings: Array<TrainingModel>) {}
}

export class AddTrainingAction implements Action {
  readonly type = TrainingListActionTypes.ADD_TRAINING;
  constructor(public training: TrainingModel) {}
}

export class AddTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.ADD_TRAINING_COMPLETED;
  constructor(public training: TrainingModel) {}
}

export class DuplicateTrainingAction implements Action {
  readonly type = TrainingListActionTypes.DUPLICATE_TRAINING;
  constructor(public training: TrainingModel) {}
}

export class DuplicateTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.DUPLICATE_TRAINING_COMPLETED;
  constructor(public training: TrainingModel) {}
}

export class UpdateTrainingAction implements Action {
  readonly type = TrainingListActionTypes.UPDATE_TRAINING;
  constructor(public training: TrainingModel) {}
}

export class UpdateTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.UPDATE_TRAINING_COMPLETED;
  constructor(public training: TrainingModel) {}
}

export class AddExerciseToTrainingAction implements Action {
  readonly type = TrainingListActionTypes.ADD_EXERCISE_TO_TRAINING;
  constructor(public trainingId: string, public exerciseId: string) {}
}

export class AddExerciseToTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.ADD_EXERCISE_TO_TRAINING_COMPLETED;
  constructor(public training: TrainingModel) {}
}

export class UpdateExerciseTrainingAction implements Action {
  readonly type = TrainingListActionTypes.UPDATE_EXERCISE_TO_TRAINING;
  constructor(public trainingId: string, public exercise: TrainingExercise) {}
}

export class UpdateExerciseTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.UPDATE_EXERCISE_TO_TRAINING_COMPLETED;
  constructor(public training: TrainingModel) {}
}

export class DeleteExerciseToTrainingAction implements Action {
  readonly type = TrainingListActionTypes.DELETE_EXERCISE_TO_TRAINING;
  constructor(public trainingId: string, public exerciseId: string) {}
}

export class DeleteExerciseToTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.DELETE_EXERCISE_TO_TRAINING_COMPLETED;
  constructor(public training: TrainingModel) {}
}

export class DeleteTrainingAction implements Action {
  readonly type = TrainingListActionTypes.DELETE_TRAINING;
  constructor(public trainingId: string) {}
}

export class DeleteTrainingCompletedAction implements Action {
  readonly type = TrainingListActionTypes.DELETE_TRAINING_COMPLETED;
  constructor(public trainingId: string) {}
}

export class NoAction implements Action {
  readonly type = TrainingListActionTypes.NO_ACTION;
}

export class ErrorAction implements Action {
  readonly type = TrainingListActionTypes.ERROR_ACTION;
}

export type TrainingListActions =
  | GetTrainingListAction
  | GetTrainingListCompletedAction
  | AddTrainingAction
  | AddTrainingCompletedAction
  | DuplicateTrainingAction
  | DuplicateTrainingCompletedAction
  | UpdateTrainingAction
  | UpdateTrainingCompletedAction
  | DeleteTrainingAction
  | DeleteTrainingCompletedAction
  | AddExerciseToTrainingAction
  | AddExerciseToTrainingCompletedAction
  | UpdateExerciseTrainingAction
  | UpdateExerciseTrainingCompletedAction
  | DeleteExerciseToTrainingAction
  | DeleteExerciseToTrainingCompletedAction
  | ErrorAction
  | NoAction;
