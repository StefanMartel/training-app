import { TrainingListActions, TrainingListActionTypes } from './training.action';
import { initialState, TrainingState } from './training.state';

export const trainingReducer = (state: TrainingState = initialState, action: TrainingListActions) => {
  switch (action.type) {
    case TrainingListActionTypes.GET_TRAINING_LIST:
      return { ...state, loading: true };
    case TrainingListActionTypes.GET_TRAINING_LIST_COMPLETED:
      return { ...state, trainings: action.trainings, loading: false };
    case TrainingListActionTypes.ADD_TRAINING_COMPLETED:
    case TrainingListActionTypes.DUPLICATE_TRAINING_COMPLETED:
      return { ...state, trainings: [...state.trainings, action.training], loading: false };
    case TrainingListActionTypes.ADD_TRAINING:
    case TrainingListActionTypes.DUPLICATE_TRAINING:
    case TrainingListActionTypes.UPDATE_TRAINING:
    case TrainingListActionTypes.DELETE_TRAINING:
    case TrainingListActionTypes.ADD_EXERCISE_TO_TRAINING:
    case TrainingListActionTypes.UPDATE_EXERCISE_TO_TRAINING:
    case TrainingListActionTypes.DELETE_EXERCISE_TO_TRAINING:
      return { ...state, loading: true };
    case TrainingListActionTypes.DELETE_TRAINING_COMPLETED:
      return { ...state, trainings: state.trainings.filter(el => el.id !== action.trainingId), loading: false };
    case TrainingListActionTypes.ADD_EXERCISE_TO_TRAINING_COMPLETED:
    case TrainingListActionTypes.UPDATE_TRAINING_COMPLETED:
    case TrainingListActionTypes.UPDATE_EXERCISE_TO_TRAINING_COMPLETED:
    case TrainingListActionTypes.DELETE_EXERCISE_TO_TRAINING_COMPLETED:
      return { ...state, trainings: [...state.trainings.filter(el => el.id !== action.training.id), action.training], loading: false };
    case TrainingListActionTypes.ERROR_ACTION:
      return { ...state, loading: false };
    default:
      return state;
  }
};


