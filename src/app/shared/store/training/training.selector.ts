import { createSelector } from '@ngrx/store';

import {TrainingExercise, TrainingModel} from '../../models/training.model';

export const selectTrainingListState = (state: any) => {
  return state.reducers.training;
};

export const selectTrainingById = (id: string) =>
  createSelector(selectTrainingListState, trainings => {
    return trainings.trainings.filter((training: TrainingModel) => training.id === id)[0];
  });

export const selectTrainingsByExerciseId = (id: string) =>
    createSelector(selectTrainingListState, trainings => {
        const filtered: TrainingModel[] = trainings.trainings.filter(
            (training: TrainingModel) => training.exercises?.find((exercise: TrainingExercise) => exercise.exerciseId === id)
        );
        return filtered;
    });

export const selectTrainingsByUser = (login: string) => createSelector(selectTrainingListState, trainings => {
  const filtered: TrainingModel[] = trainings.trainings.filter((training: TrainingModel) => training.user === login);
  return filtered.length > 0 ? filtered : [];
});

export const isTrainingLoading = createSelector(selectTrainingListState, trainings => {
   return trainings.loading;
});
