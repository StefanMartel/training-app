import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';

import {
    TrainingListActionTypes,
    AddTrainingAction,
    GetTrainingListAction,
    GetTrainingListCompletedAction,
    AddTrainingCompletedAction,
    DeleteTrainingAction,
    DeleteTrainingCompletedAction,
    AddExerciseToTrainingCompletedAction,
    AddExerciseToTrainingAction,
    DeleteExerciseToTrainingAction,
    DeleteExerciseToTrainingCompletedAction,
    UpdateExerciseTrainingAction,
    UpdateExerciseTrainingCompletedAction,
    UpdateTrainingAction,
    UpdateTrainingCompletedAction,
    DuplicateTrainingCompletedAction,
    ErrorAction,
} from './training.action';
import { TrainingService } from '../../services/training.service';
import { InfoDisplayService } from '../../services/info-display.service';
import { typeOfMessageToDisplay } from '../../models/enums/message-to-display.model';
import { TrainingModel } from '../../models/training.model';

@Injectable()
export class TrainingEffects {
  constructor(
    private actions$: Actions,
    private trainingService: TrainingService,
    private infoDisplayService: InfoDisplayService,
    private translate: TranslateService,
  ) {}

  getTrainingList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TrainingListActionTypes.GET_TRAINING_LIST),
      switchMap((option: GetTrainingListAction) => {
        return this.trainingService.getTrainingListByUser(option.userLogin).pipe(
          map((trainings: TrainingModel[]) => {
            return new GetTrainingListCompletedAction(trainings);
          }),
          catchError(() => {
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  addTraining$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TrainingListActionTypes.ADD_TRAINING),
      switchMap((option: AddTrainingAction) => {
        return this.trainingService.addTrainingBack(option.training).pipe(
          map(dataReturn => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.ADD_TRAINING'));
            return new AddTrainingCompletedAction(dataReturn);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.ADD_TRAINING_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

    duplicateTraining$ = createEffect(() =>
        this.actions$.pipe(
            ofType(TrainingListActionTypes.DUPLICATE_TRAINING),
            switchMap((option: AddTrainingAction) => {
                return this.trainingService.duplicateTrainingBack(option.training).pipe(
                    map(dataReturn => {
                        this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.DUPLICATE_TRAINING'));
                        return new DuplicateTrainingCompletedAction(dataReturn);
                    }),
                    catchError(() => {
                        this.infoDisplayService.displaySnackBarMessage(
                            this.translate.instant('CONFIRM.ACTION.DUPLICATE_TRAINING_ERROR'),
                            typeOfMessageToDisplay.error,
                        );
                        return of(new ErrorAction());
                    }),
                );
            }),
        ),
    );

    updateTraining$ = createEffect(() =>
        this.actions$.pipe(
            ofType(TrainingListActionTypes.UPDATE_TRAINING),
            switchMap((option: UpdateTrainingAction) => {
                return this.trainingService.updateTrainingBack(option.training).pipe(
                    map(dataReturn => {
                        this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.UPDATE_TRAINING'));
                        return new UpdateTrainingCompletedAction(dataReturn);
                    }),
                    catchError(() => {
                        this.infoDisplayService.displaySnackBarMessage(
                            this.translate.instant('CONFIRM.ACTION.UPDATE_TRAINING_ERROR'),
                            typeOfMessageToDisplay.error,
                        );
                        return of(new ErrorAction());
                    }),
                );
            }),
        ),
    );

  addExerciseToTraining$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TrainingListActionTypes.ADD_EXERCISE_TO_TRAINING),
      switchMap((option: AddExerciseToTrainingAction) => {
        return this.trainingService.addExerciseToTrainingBack(option.trainingId, option.exerciseId).pipe(
          map((dataReturn: TrainingModel) => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.ADD_EXERCISE_TO_TRAINING'));
            return new AddExerciseToTrainingCompletedAction(dataReturn);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.ADD_EXERCISE_TO_TRAINING_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  updateExerciseTraining$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TrainingListActionTypes.UPDATE_EXERCISE_TO_TRAINING),
      switchMap((option: UpdateExerciseTrainingAction) => {
        return this.trainingService.updateExerciseToTrainingBack(option.trainingId, option.exercise).pipe(
          map((dataReturn: TrainingModel) => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.ADD_EXERCISE_TO_TRAINING'));
            return new UpdateExerciseTrainingCompletedAction(dataReturn);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.ADD_EXERCISE_TO_TRAINING_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  deleteExerciseToTraining$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TrainingListActionTypes.DELETE_EXERCISE_TO_TRAINING),
      switchMap((option: DeleteExerciseToTrainingAction) => {
        return this.trainingService.deleteExerciseToTrainingBack(option.trainingId, option.exerciseId).pipe(
          map((dataReturn: TrainingModel) => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.DELETE_EXERCISE_TO_TRAINING'));
            return new DeleteExerciseToTrainingCompletedAction(dataReturn);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.DELETE_EXERCISE_TO_TRAINING_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  deleteTraining$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TrainingListActionTypes.DELETE_TRAINING),
      switchMap((option: DeleteTrainingAction) => {
        return this.trainingService.deleteTrainingBack(option.trainingId).pipe(
          map(() => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.DELETE', { objet: 'Scéance' }));
            return new DeleteTrainingCompletedAction(option.trainingId);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.DELETE_TRAINING_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );
}
