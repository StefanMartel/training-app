import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ExerciseModel } from '../../models/exercise.model';

export const featureAdapter: EntityAdapter<ExerciseModel> = createEntityAdapter<ExerciseModel>({
  selectId: model => model.id,
  sortComparer: (a: ExerciseModel, b: ExerciseModel): number => b.creationDate.toString().localeCompare(a.creationDate.toString()),
});

export interface ExerciseState extends EntityState<ExerciseModel> {
  exercises: Array<ExerciseModel>;
  loading: boolean;
}

export const initialState: ExerciseState = featureAdapter.getInitialState({
  exercises: [],
  loading: false,
});
