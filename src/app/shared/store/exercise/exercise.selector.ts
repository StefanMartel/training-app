import { createSelector } from '@ngrx/store';

import { ExerciseModel } from '../../models/exercise.model';

export const selectExerciseListState = (state: any) => {
  return state.reducers.exercise;
};

export const selectExerciseById = (id: string) =>
  createSelector(selectExerciseListState, exercises => exercises.exercises.filter((exercise: ExerciseModel) => exercise.id === id)[0]);

export const selectExercises = createSelector(selectExerciseListState, exercises => {
  return Array.isArray(exercises.exercises) ? exercises.exercises : [];
});

export const isExerciseLoading = createSelector(selectExerciseListState, exercises => {
  return exercises.loading;
});
