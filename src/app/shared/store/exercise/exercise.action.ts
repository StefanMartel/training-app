import { Action } from '@ngrx/store';
import { ExerciseModel } from '../../models/exercise.model';

export enum ExerciseListActionTypes {
  GET_EXERCISE_LIST = 'Exercise list get Request',
  GET_EXERCISE_LIST_COMPLETED = 'Exercise list get completed',
  ADD_EXERCISE = 'Exercise list add Request',
  ADD_EXERCISE_COMPLETED = 'Exercise list add completed',
  UPDATE_EXERCISE = 'Exercise list update Request',
  UPDATE_EXERCISE_COMPLETED = 'Exercise list update completed',
  DELETE_EXERCISE = 'Exercise delete Request',
  DELETE_EXERCISE_COMPLETED = 'Exercise delete completed',
  ERROR_ACTION = 'Action suite à erreur',
  NO_ACTION = 'Aucune action de faite ',
}

export class GetExerciseListAction implements Action {
  readonly type = ExerciseListActionTypes.GET_EXERCISE_LIST;
  constructor() {}
}

export class GetExerciseListCompletedAction implements Action {
  readonly type = ExerciseListActionTypes.GET_EXERCISE_LIST_COMPLETED;
  constructor(public exercises: Array<ExerciseModel>) {}
}

export class AddExerciseAction implements Action {
  readonly type = ExerciseListActionTypes.ADD_EXERCISE;
  constructor(public exercise: ExerciseModel) {}
}

export class AddExerciseCompletedAction implements Action {
  readonly type = ExerciseListActionTypes.ADD_EXERCISE_COMPLETED;
  constructor(public exercise: ExerciseModel) {}
}

export class UpdateExerciseAction implements Action {
  readonly type = ExerciseListActionTypes.UPDATE_EXERCISE;
  constructor(public exercise: ExerciseModel) {}
}

export class UpdateExerciseCompletedAction implements Action {
  readonly type = ExerciseListActionTypes.UPDATE_EXERCISE_COMPLETED;
  constructor(public exercise: ExerciseModel) {}
}

export class DeleteExerciseAction implements Action {
  readonly type = ExerciseListActionTypes.DELETE_EXERCISE;
  constructor(public exerciseId: string) {}
}

export class DeleteExerciseCompletedAction implements Action {
  readonly type = ExerciseListActionTypes.DELETE_EXERCISE_COMPLETED;
  constructor(public exerciseId: string) {}
}

export class ErrorAction implements Action {
  readonly type = ExerciseListActionTypes.ERROR_ACTION;
}

export class NoAction implements Action {
  readonly type = ExerciseListActionTypes.NO_ACTION;
}

export type ExerciseListActions =
  | GetExerciseListAction
  | GetExerciseListCompletedAction
  | AddExerciseAction
  | AddExerciseCompletedAction
  | UpdateExerciseAction
  | UpdateExerciseCompletedAction
  | DeleteExerciseAction
  | DeleteExerciseCompletedAction
  | ErrorAction
  | NoAction;
