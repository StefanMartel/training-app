import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';

import {
    ExerciseListActionTypes,
    AddExerciseAction,
    GetExerciseListAction,
    GetExerciseListCompletedAction,
    AddExerciseCompletedAction,
    DeleteExerciseAction,
    DeleteExerciseCompletedAction, UpdateExerciseAction, UpdateExerciseCompletedAction,
} from './exercise.action';
import { TranslateService } from '@ngx-translate/core';
import { InfoDisplayService } from '../../services/info-display.service';
import { typeOfMessageToDisplay } from '../../models/enums/message-to-display.model';
import { ExerciseService } from '../../services/exercise.service';
import { ExerciseModel } from '../../models/exercise.model';
import {ErrorAction} from '../training/training.action';

@Injectable()
export class ExerciseEffects {
  constructor(
    private actions$: Actions,
    private exerciceService: ExerciseService,
    private infoDisplayService: InfoDisplayService,
    private translate: TranslateService,
  ) {}

  getExerciseList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExerciseListActionTypes.GET_EXERCISE_LIST),
      switchMap((option: GetExerciseListAction) => {
        return this.exerciceService.getExerciseList().pipe(
          map((exercises: ExerciseModel[]) => {
            return new GetExerciseListCompletedAction(exercises);
          }),
          catchError(() => {
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

  addExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExerciseListActionTypes.ADD_EXERCISE),
      switchMap((option: AddExerciseAction) => {
        return this.exerciceService.addExerciseBack(option.exercise).pipe(
          map(dataReturn => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.ADD_EXERCISE'));
            return new AddExerciseCompletedAction(dataReturn);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.ADD_EXERCISE_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );

    updateExercise$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ExerciseListActionTypes.UPDATE_EXERCISE),
            switchMap((option: UpdateExerciseAction) => {
                return this.exerciceService.updateExerciseBack(option.exercise).pipe(
                    map(dataReturn => {
                        this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.UPDATE_EXERCISE'));
                        return new UpdateExerciseCompletedAction(dataReturn);
                    }),
                    catchError(() => {
                        this.infoDisplayService.displaySnackBarMessage(
                            this.translate.instant('CONFIRM.ACTION.UPDATE_EXERCISE_ERROR'),
                            typeOfMessageToDisplay.error,
                        );
                        return of(new ErrorAction());
                    }),
                );
            }),
        ),
    );

  deleteExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExerciseListActionTypes.DELETE_EXERCISE),
      switchMap((option: DeleteExerciseAction) => {
        return this.exerciceService.deleteExerciseBack(option.exerciseId).pipe(
          map(() => {
            this.infoDisplayService.displaySnackBarMessage(this.translate.instant('CONFIRM.ACTION.DELETE', { objet: 'Exercice' }));
            return new DeleteExerciseCompletedAction(option.exerciseId);
          }),
          catchError(() => {
            this.infoDisplayService.displaySnackBarMessage(
              this.translate.instant('CONFIRM.ACTION.DELETE_EXERCISE_ERROR'),
              typeOfMessageToDisplay.error,
            );
            return of(new ErrorAction());
          }),
        );
      }),
    ),
  );
}
