import { ExerciseListActions, ExerciseListActionTypes } from './exercise.action';
import { ExerciseState, initialState } from './exercise.state';

export const exerciseReducer = (state: ExerciseState = initialState, action: ExerciseListActions) => {
  switch (action.type) {
    case ExerciseListActionTypes.GET_EXERCISE_LIST:
      return { ...state, loading: true };
    case ExerciseListActionTypes.GET_EXERCISE_LIST_COMPLETED:
      return { ...state, exercises: action.exercises, loading: false };
    case ExerciseListActionTypes.ADD_EXERCISE_COMPLETED:
    case ExerciseListActionTypes.UPDATE_EXERCISE_COMPLETED:
      return { ...state, exercises: [...state.exercises.filter(el => el.id !== action.exercise.id), action.exercise], loading: false };
    case ExerciseListActionTypes.ADD_EXERCISE:
    case ExerciseListActionTypes.UPDATE_EXERCISE:
    case ExerciseListActionTypes.DELETE_EXERCISE:
      return { ...state, loading: true };
    case ExerciseListActionTypes.DELETE_EXERCISE_COMPLETED:
      return { ...state, exercises: state.exercises.filter(el => el.id !== action.exerciseId), loading: false };
    case ExerciseListActionTypes.ERROR_ACTION:
      return { ...state, loading: false };
    default:
      return state;
  }
};
