export const BASE_REP = 8;

export const returnCode = {
  bddError: { code: 500, message: 'Probléme BDD' },
  inserted: { code: 201, message: 'OK' },
  deleted: { code: 204, message: 'OK' },
  badRequest: { code: 400, message: 'Bad Request' },
  noData: { code: 401, message: 'No data found' },
  noMoreVerificationCodeAttempt: { code: 405, message: 'No more verification code attempt to use' },
  notGoodVerificationCode: { code: 406, message: 'Not a good verification code' },
};
