import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { MaterialModule } from './material.module';
import { LoadingComponent } from './components/loading/loading.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

@NgModule({
  declarations: [SideMenuComponent, ListItemComponent, LoadingComponent],
  imports: [
    CommonModule,
    IonicModule,
    HttpClientModule,
    TranslateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HighchartsChartModule,
    //AgmCoreModule.forRoot({ apiKey: 'AIzaSyANKiSPp_Iyp-UFFsQxtKeV3D3BjT2vnC0' }),
  ],
  exports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SideMenuComponent,
    ListItemComponent,
    HighchartsChartModule,
    LoadingComponent,
  ],
})
export class SharedModule {
  constructor(translate: TranslateService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('fr');

    const browserLang = translate.getBrowserLang() || 'fr';
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'fr');
  }
}
