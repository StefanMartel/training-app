import { TrainingModel } from '../models/training.model';

export const TrainingListMock: TrainingModel[] = [
  {
    user: '',
    id: 0,
    title: 'Training 1',
    creationDate: new Date(),
  },
  {
    user: 'SM',
    id: 1,
    title: 'Training 2',
    creationDate: new Date(),
  },
  {
    user: 'SM',
    id: 2,
    title: 'Training 3',
    creationDate: new Date(),
  },
];
