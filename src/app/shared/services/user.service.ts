import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {map, take} from 'rxjs/operators';
import { Observable } from 'rxjs';
import {isUserLoading, selectUser} from '../store/user/user.selector';
import { HttpRepo } from '../http/http';
import { UrlConfiguration } from '../http/configuration/url.configuration';
import { selectExercises } from '../store/exercise/exercise.selector';
import { ExerciseModel } from '../models/exercise.model';
import {UserModel} from '../models/user.model';
import {
  ChangePasswordAction,
  ConnectUserAction,
  DisconnectUserAction,
  ResetPasswordAction,
  ValidateCodeAction,
} from '../store/user/user.action';
import {ErrorResponse} from '../models/error-response.model';
import {ReturnResponseModel} from '../models/return-response.model';

@Injectable()
export class UserService {
  currentUser: string;
  currentEmail: string;
  isLoading$: Observable<boolean>;

  getExerciseList$: Observable<ExerciseModel[]> = this.store.select(selectExercises);

  constructor(public store: Store<any>, public httpRepo: HttpRepo, private urlConfiguration: UrlConfiguration) {
    this.currentUser = '';
    this.currentEmail = '';
    this.store.select(selectUser).subscribe(user => {
      console.log('USER', user);
      if (user) {
        this.currentUser = user.login;
      }
    });
    this.isLoading$ = this.store.select(isUserLoading);
  }

  connectUser(login: string, password: string): void {
    return this.store.dispatch(new ConnectUserAction(login, password));
  }

  connectUserBack(login: string, password: string): Observable<UserModel | ErrorResponse> {
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('connectUser', [login, password])).pipe(map(data => data));
  }

  disconnectUser(): void {
    this.store.select(selectUser).pipe(take(1)).subscribe(
        user => this.store.dispatch(new DisconnectUserAction(user.login))
    )
  }

  disconnectUserBack(login: string): Observable<ReturnResponseModel | ErrorResponse> {
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('disconnectUser', [login])).pipe(map(data => data));
  }

  resetPassword(email: string): void {
    this.currentEmail = email;
    return this.store.dispatch(new ResetPasswordAction(email));
  }

  resetPasswordBack(email: string): Observable<ReturnResponseModel | ErrorResponse> {
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('resetPassword', [email])).pipe(map(data => data));
  }

  validateCode(code: string): void {
    return this.store.dispatch(new ValidateCodeAction(code));
  }

  validateCodeBack(code: string): Observable<ReturnResponseModel | ErrorResponse> {
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('validateCode', [this.currentEmail, code])).pipe(map(data => data));
  }

  changePassword(password: string): void {
    return this.store.dispatch(new ChangePasswordAction(password));
  }

  changePasswordBack(password: string): Observable<ReturnResponseModel | ErrorResponse> {
    console.log('CHANGE', this.currentEmail, password);
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('changePassword', [this.currentEmail, password])).pipe(map(data => data));
  }
}
