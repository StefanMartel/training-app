import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MapService {
  coordinates: any;

  constructor() {}

  public getPosition(): Observable<any> {
    return new Observable((observer: { next: (arg0: any) => void }) => {
      navigator.geolocation.watchPosition((pos: any) => {
        observer.next(pos);
      });
    });
  }
}
