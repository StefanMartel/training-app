import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TrainingService} from './training.service';
import {ExerciseService} from './exercise.service';
import {combineLatest} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class LoadingService {

    constructor(
        private trainingService: TrainingService,
        private exerciseService: ExerciseService
    ){}

    isLoading(): Observable<boolean> {
        return combineLatest([this.trainingService.isLoading$, this.exerciseService.isLoading$]).pipe(
            map(([trainingLoading, exerciseLoading]) => trainingLoading || exerciseLoading)
        )
    }

}
