import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {
  selectTrainingsByUser,
  selectTrainingById,
  isTrainingLoading,
  selectTrainingsByExerciseId
} from '../store/training/training.selector';
import {
  AddExerciseToTrainingAction,
  AddTrainingAction,
  DeleteExerciseToTrainingAction,
  DeleteTrainingAction, DuplicateTrainingAction,
  UpdateExerciseTrainingAction, UpdateTrainingAction,
} from '../store/training/training.action';
import { selectUser } from '../store/user/user.selector';
import { HttpRepo } from '../http/http';
import { UrlConfiguration } from '../http/configuration/url.configuration';
import { TrainingExercise, TrainingModel } from '../models/training.model';
import {UserModel} from '../models/user.model';

@Injectable()
export class TrainingService {
  currentUser: string;
  isLoading$: Observable<boolean>;

  getTrainingList$: Observable<TrainingModel[]> = of([]);

  constructor(public store: Store<any>, public httpRepo: HttpRepo, private urlConfiguration: UrlConfiguration) {
    this.currentUser = '';
    this.store.select(selectUser).subscribe((user : UserModel) => {
      if (user) {
        this.currentUser = user.login;
        this.getTrainingList$ = this.store.select(selectTrainingsByUser(user.login));
      }
    });
    this.isLoading$ = this.store.select(isTrainingLoading);
  }

  getTrainingListByUser(userLogin: string): Observable<TrainingModel[]> {
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('getTrainingListByUser', [userLogin]));
  }

  getTrainingById(id: string): Observable<TrainingModel> {
    return this.store.select(selectTrainingById(id));
  }

  getTrainingsByExerciceId(id: string): Observable<TrainingModel[]> {
    return this.store.select(selectTrainingsByExerciseId(id));
  }

  addTraining(training: string) {
    this.store.dispatch(
        new AddTrainingAction({
          user: this.currentUser,
          id: '',
          title: training,
          creationDate: new Date(),
          exercises: [],
        }),
    );
  }

  addTrainingBack(training: TrainingModel): Observable<any> {
    return this.httpRepo.httpCallPost(this.urlConfiguration.getBackEndUrl('addTraining'), training).pipe(map(data => data.body));
  }

  duplicateTraining(training: TrainingModel){
    this.store.dispatch(
        new DuplicateTrainingAction(training)
    );
  }

  duplicateTrainingBack(training: TrainingModel): Observable<any> {
    return this.httpRepo.httpCallPost(this.urlConfiguration.getBackEndUrl('duplicateTraining'), training).pipe(map(data => data.body));
  }

  updateTraining(training: TrainingModel) {
    this.store.dispatch(new UpdateTrainingAction(training));
  }

  updateTrainingBack(training: TrainingModel): Observable<any> {
    return this.httpRepo.httpCallPost(this.urlConfiguration.getBackEndUrl('updateTraining'), training).pipe(map(data => data.body));
  }

  addExerciseToTraining(trainingId: string, exerciseId: string) {
    this.store.dispatch(new AddExerciseToTrainingAction(trainingId, exerciseId));
  }

  addExerciseToTrainingBack(trainingId: string, exerciseId: string): Observable<any> {
    const trainingExercise: Partial<TrainingExercise> = { exerciseId };
    return this.httpRepo
      .httpCallPost(this.urlConfiguration.getBackEndUrl('addExerciseToTraining', [trainingId]), trainingExercise)
      .pipe(map(data => data.body));
  }

  updateExerciseTraining(trainingId: string, exercise: TrainingExercise) {
    this.store.dispatch(new UpdateExerciseTrainingAction(trainingId, exercise));
  }

  updateExerciseToTrainingBack(trainingId: string, exercise: TrainingExercise): Observable<any> {
    return this.httpRepo
      .httpCallPost(this.urlConfiguration.getBackEndUrl('updateExerciseTraining', [trainingId]), exercise)
      .pipe(map(data => data.body));
  }

  deleteExerciseToTraining(trainingId: string, exerciseId: string) {
    this.store.dispatch(new DeleteExerciseToTrainingAction(trainingId, exerciseId));
  }

  deleteExerciseToTrainingBack(trainingId: string, exerciseId: string): Observable<any> {
    return this.httpRepo
      .httpCallDelete(this.urlConfiguration.getBackEndUrl('deleteExerciseToTraining', [trainingId, exerciseId]))
      .pipe(map(data => data.body));
  }

  deleteTraining(trainingId: string) {
    this.store.dispatch(new DeleteTrainingAction(trainingId));
  }

  deleteTrainingBack(trainingId: string): Observable<any> {
    return this.httpRepo.httpCallDelete(this.urlConfiguration.getBackEndUrl('deleteTrainingByTrainingId', [trainingId]));
  }
}
