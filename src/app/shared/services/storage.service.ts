import { Preferences } from '@capacitor/preferences';
import { Injectable } from '@angular/core';

export enum StorageType {
  USER = 'user',
  CONNECTED_WATCH = 'connected watch',
}

@Injectable()
export class StorageService {
  static setObject(key: string, value: any): Promise<void> {
    console.log('SET OBJECT', key, value);
    return Preferences.set({
      key,
      value: JSON.stringify(value),
    });
  }

  static getObject(key: StorageType): Promise<any> {
    return Preferences.get({ key }).then(value => {
      console.log('VALUE GET', value, value.value, JSON.parse(value.value!));
      return value.value ? JSON.parse(value.value) : null;
    });
  }
}
