import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { selectUser } from '../store/user/user.selector';
import { HttpRepo } from '../http/http';
import { UrlConfiguration } from '../http/configuration/url.configuration';
import { isExerciseLoading, selectExerciseById, selectExercises } from '../store/exercise/exercise.selector';
import { AddExerciseAction, DeleteExerciseAction, UpdateExerciseAction } from '../store/exercise/exercise.action';
import { ExerciseModel } from '../models/exercise.model';

@Injectable()
export class ExerciseService {
  currentUser: string;
  isLoading$: Observable<boolean>;

  getExerciseList$: Observable<ExerciseModel[]> = this.store.select(selectExercises);

  constructor(public store: Store<any>, public httpRepo: HttpRepo, private urlConfiguration: UrlConfiguration) {
    this.currentUser = '';
    this.store.select(selectUser).subscribe(user => {
      if (user) {
        this.currentUser = user.login;
      }
    });
    this.isLoading$ = this.store.select(isExerciseLoading);
  }

  getExerciseList(): Observable<ExerciseModel[]> {
    return this.httpRepo.httpCallGet(this.urlConfiguration.getBackEndUrl('getExerciseList')).pipe(
      map((ex: ExerciseModel[]) =>
        ex.sort((a, b) => {
          if (a.title < b.title) {
            return -1;
          }
          return 1;
        }),
      ),
    );
  }

  getExerciseById(id: string): Observable<ExerciseModel> {
    return this.store.select(selectExerciseById(id));
  }

  addExercise(trainingLabel: string) {
    this.store.dispatch(
      new AddExerciseAction({
        id: '',
        title: trainingLabel,
        imgLink: '',
        videoLink: '',
        creationDate: new Date(),
      }),
    );
  }

  addExerciseBack(exercise: ExerciseModel): Observable<any> {
    return this.httpRepo.httpCallPost(this.urlConfiguration.getBackEndUrl('addExercise'), exercise).pipe(map(data => data.body));
  }

  updateExercise(exercise: ExerciseModel) {
    this.store.dispatch(new UpdateExerciseAction(exercise));
  }

  updateExerciseBack(exercise: ExerciseModel): Observable<any> {
    return this.httpRepo.httpCallPost(this.urlConfiguration.getBackEndUrl('updateExercise'), exercise).pipe(map(data => data.body));
  }

  deleteExercise(exerciseId: string) {
    this.store.dispatch(new DeleteExerciseAction(exerciseId));
  }

  deleteExerciseBack(exerciseId: string): Observable<any> {
    return this.httpRepo.httpCallDelete(this.urlConfiguration.getBackEndUrl('deleteExerciseByExerciseId', [exerciseId]));
  }
}
