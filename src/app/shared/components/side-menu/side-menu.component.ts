import { Component } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import {Store} from '@ngrx/store';
import {selectUser} from '../../store/user/user.selector';
//import {StorageService, StorageType} from "../../services/storage.service";

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent {
  active = '';
  navs = [
    {
      name: 'Scéances',
      link: '/trainings',
      icon: 'albums'
    },
    {
      name: 'Progression',
      link: '/progression',
      icon: 'analytics'
    },
    {
      name: 'Exercices',
      link: '/exercises',
      icon: 'albums'
    },
    {
      name: 'Connection',
      action: () => {return this.connectBluetooth();},
      icon: 'bluetooth'
    },
    {
      name: 'Déconnection',
      action: () => {return this.disconnectBluetooth();},
      icon: 'bluetooth'
    },
    {
      name: 'Déconnection',
      link: '/user/logout',
      icon: 'trash'
    }
  ];
  name = '';

  constructor(
      private router: Router,
      private store: Store
  ) {
    // @ts-ignore
    this.router.events.subscribe((event: RouterEvent) => {
      this.active = event.url;
    });
    this.store.select(selectUser).subscribe(
        user => this.name = user ? (user.firstName + ' ' + user.lastName) : ''
    )
  }

  navigateTo(path: string) {
    this.router.navigate([path]);
  }

  async connectBluetooth() {
   /* await BleClient.initialize();
    BleClient.requestDevice().then( (result: BleDevice) => {
      StorageService.setObject(StorageType.CONNECTED_WATCH, result);
      console.log('SCAN RESULT', result)
    })*/
  }

  async disconnectBluetooth() {
   /* await BleClient.initialize();

    BleClient.disconnect(await StorageService.getObject(StorageType.CONNECTED_WATCH))
    BleClient.requestDevice().then( (result: BleDevice) => {
      StorageService.setObject(StorageType.CONNECTED_WATCH, result);
          console.log('SCAN RESULT', result)
        }
    )*/
  }
}
