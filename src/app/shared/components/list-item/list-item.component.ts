import { Component, Input, Output, EventEmitter } from '@angular/core';

export interface ListItemDisplay {
  title: string;
  id: string;
}

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
})
export class ListItemComponent {
  @Input() item?: ListItemDisplay;

  @Output() deleteItemId = new EventEmitter<string>();
  @Output() selectItemId = new EventEmitter<string>();
  @Output() duplicateItem = new EventEmitter<string>()

  constructor() {}

  selectItem(trainingId: string) {
    this.selectItemId.emit(trainingId);
  }

  deleteTraining(trainingId: string, event: Event) {
    event.stopPropagation();
    this.deleteItemId.emit(trainingId);
  }

  duplicateTraining(trainingId: string, event: Event){
    event.stopPropagation();
    this.duplicateItem.emit(trainingId);
  }
}
