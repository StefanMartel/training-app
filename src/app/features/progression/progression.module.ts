import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { IsSignedInGuard } from '../user/user-guard';
import { ProgressionPage } from './pages/progression/progression.page';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
          { path: '', pathMatch: 'full', component: ProgressionPage, canActivate: [IsSignedInGuard] },
    ]),
  ],
  declarations: [ProgressionPage]
})
export class ProgressionPageModule {}
