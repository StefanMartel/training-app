import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import * as Highcharts from 'highcharts';

import { ExerciseModel } from '../../../../shared/models/exercise.model';
import { ExerciseService } from '../../../../shared/services/exercise.service';
import { TrainingService } from '../../../../shared/services/training.service';
import { TrainingExercise, TrainingModel } from '../../../../shared/models/training.model';
import { BASE_REP } from '../../../../shared/util';

@Component({
  selector: 'app-progression',
  templateUrl: './progression.page.html',
  styleUrls: ['./progression.page.scss'],
})
export class ProgressionPage {
  exerciseList$: Observable<ExerciseModel[]> = this.exerciseService.getExerciseList$;
  _Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};
  chartWidth = 300;

  // HighChart data
  displayChart = false;
  listOfDate: string[] = [];
  seriesOfValues: Highcharts.SeriesOptionsType[] = [];

  updateFromInput = true;

  constructor(private exerciseService: ExerciseService, private trainingService: TrainingService) {}

  changeExercice(exerciseId: string) {
    this.displayChart = true;
    this.trainingService.getTrainingsByExerciceId(exerciseId).subscribe((data: TrainingModel[]) => {
      this.listOfDate = [];
      this.seriesOfValues = [];
      let exerciseLabel;
      let unityLabel = 'Kg';
      let unityLongLabel = 'Poids';
      data.forEach(value => {
        this.listOfDate.push(new Date(value.creationDate).toLocaleDateString());
        const exerciceToDisplay: TrainingExercise = value.exercises!.find(exercise => exercise.exerciseId === exerciseId)!;
        exerciseLabel = exerciceToDisplay.exerciseLabel;
        if (exerciceToDisplay.reps) {
          for (let i = 0; i < exerciceToDisplay.reps.length; i++) {
            if (!this.seriesOfValues[i]) {
              this.seriesOfValues[i] = {
                type: 'line',
                name: 'rep ' + (+i + 1),
                data: [],
              };
            }
            if (exerciceToDisplay.reps) {
              if (exerciceToDisplay.reps[i].weight) {
                //@ts-ignore
                this.seriesOfValues[i].data.push((+exerciceToDisplay.reps![i].weight * +exerciceToDisplay.reps![i].reps) / BASE_REP);
              } else if (exerciceToDisplay.reps[i].distance) {
                //@ts-ignore
                this.seriesOfValues[i].data.push(+exerciceToDisplay.reps![i].distance);
                unityLabel = 'm';
                unityLongLabel = 'distance';
              } else if (exerciceToDisplay.reps[i].reps) {
                //@ts-ignore
                this.seriesOfValues[i].data.push(+exerciceToDisplay.reps![i].reps);
                unityLabel = '';
                unityLongLabel = 'reps';
              } else if (exerciceToDisplay.reps[i].time) {
                //@ts-ignore
                this.seriesOfValues[i].data.push(+exerciceToDisplay.reps![i].time);
                unityLabel = 's';
                unityLongLabel = 'time';
              }
            }
          }
        }
      });
      this.chartOptions = {
        title: {
          text: exerciseLabel,
          style: {
            color: '#FFFFFF',
            fontWeight: 'bold',
          },
        },
        legend: {
          itemStyle: { color: '#F00' },
        },
        chart: {
          style: {
            color: '#F00',
          },
          width: 360,
          backgroundColor: '#FFF',
        },
        colors: ['#FF0000', '#FF7F00', '#FFFF00'],
        series: this.seriesOfValues,
        xAxis: {
          lineWidth: 0,
          gridLineWidth: 1,
          gridLineColor: 'rgba(255, 0, 0, 0.25)',
          lineColor: '#F00',
          tickColor: '#F00',
          labels: {
            style: {
              color: '#F00',
            },
          },
          categories: this.listOfDate,
        },
        yAxis: {
          lineWidth: 0,
          gridLineWidth: 1,
          gridLineColor: 'rgba(255, 0, 0, 0.25)',
          lineColor: '#F00',
          tickColor: '#F00',
          labels: {
            style: {
              color: '#F00',
            },
          },
          title: {
            style: {
              color: '#F00',
            },
            text: unityLongLabel + ' (' + unityLabel + ')',
          },
        },
        tooltip: {
          valueDecimals: 0,
          valueSuffix: unityLabel,
        },
        credits: {
          enabled: false,
        },
      };
      this.updateFromInput = true;
    });
  }
}
