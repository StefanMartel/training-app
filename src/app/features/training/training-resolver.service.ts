import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TrainingService } from '../../shared/services/training.service';
import { catchError, first, map } from 'rxjs/operators';
import { TrainingModelResolver, TrainingsModelResolver } from '../../shared/models/training.model';

@Injectable()
export class TrainingResolver {
  constructor(private trainingService: TrainingService) {}
  resolve(): Observable<TrainingsModelResolver> {
    return this.trainingService.getTrainingList$.pipe(
      first(),
      map(trainings => {
        return { data: trainings } as TrainingsModelResolver;
      }),
      catchError(error => {
        const message = 'Message Error' + error;
        return of({ data: [], error: message } as TrainingsModelResolver);
      }),
    );
  }
}

@Injectable()
export class TrainingByIdResolver {
  constructor(private trainingService: TrainingService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<TrainingModelResolver> {
    const id = route.params['trainingId'];
    if (isNaN(id)) {
      return of({ error: 'NOT A GOOD ID' } as TrainingModelResolver);
    }
    return this.trainingService.getTrainingById(id).pipe(
      first(),
      map(training => {
        return { data: training } as TrainingModelResolver;
      }),
      catchError(error => {
        const message = 'Message Error' + error;
        return of({ error: message } as TrainingModelResolver);
      }),
    );
  }
}
