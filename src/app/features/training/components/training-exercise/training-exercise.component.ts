import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import { TrainingExercise, TrainingExerciseRep } from '../../../../shared/models/training.model';
import { RepComponent } from '../rep/rep.component';
import { AlertController } from '@ionic/angular';

enum TypeOfExerciceUpdate {
  REPS = 'reps',
  WEIGHT = 'weight',
  TIME = 'time',
  DISTANCE = 'distance',
  KCAL = 'kcal',
}

@Component({
  selector: 'app-training-exercise',
  templateUrl: './training-exercise.component.html',
  styleUrls: ['./training-exercise.component.scss'],
})
export class TrainingExerciseComponent {
  @Input() exercise?: TrainingExercise;
  @Input() selected = false;
  @Output() repAdded = new EventEmitter<TrainingExercise>();
  @Output() repUpdated = new EventEmitter<TrainingExercise>();
  @Output() exerciseDeleted = new EventEmitter<string>();
  @Output() sendExpanded = new EventEmitter<boolean>();

  typeOfExerciceUpdate = TypeOfExerciceUpdate;
  deleteConfirm$ = new Subject();
  state = {
    newRep: {
      add: false,
      exerciceId: '',
    },
  };
  expanded = false;

  constructor(private dialog: MatDialog, private translate: TranslateService, private alertController: AlertController) {}

  openRepModal(repId?: number) {
    const dialogRef = this.dialog.open(RepComponent, {
      width: '450px',
      data: { title: this.exercise?.exerciseLabel, rep: typeof repId === 'number' ? this.exercise!.reps![repId] : null },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result[1] === 0) {
        this.validateNewRep(result[0]);
      }
      if (result && result[1] === 1 && typeof repId === 'number') {
        this.updateRep(result[0], repId);
      }
      if (result && result[1] === 2 && typeof repId === 'number') {
        this.deleteRep(repId);
      }
    });
  }

  validateNewRep(rep: TrainingExerciseRep): void {
    if (this.exercise) {
      if (this.exercise?.reps) {
        this.exercise.reps.push(rep);
      } else {
        this.exercise.reps = [rep];
      }
      this.repAdded.emit(this.exercise);
    }
    this.state.newRep.add = false;
  }

  updateRep(rep: TrainingExerciseRep, repIndex: number): void {
    if (this.exercise && this.exercise.reps) {
      this.exercise!.reps[repIndex] = rep;
      this.repUpdated.emit(this.exercise);
    }
  }

  deleteRep(repIndex: number): void {
    this.exercise?.reps?.splice(repIndex, 1);
    this.repUpdated.emit(this.exercise);
  }

  deleteExercise(): void {
    this.deleteConfirm$.next(false);
    this.openDialog();
    this.deleteConfirm$.subscribe(data => {
      if (data) {
        this.exerciseDeleted.emit(this.exercise?.exerciseId);
      }
    });
  }

  async openDialog() {
    const alert = await this.alertController.create({
      header: '',
      message: this.translate.instant('CONFIRM.DELETE_EXERCISE'),
      buttons: [
        {
          text: this.translate.instant('CONFIRM.BUTTON.CANCEL'),
        },
        {
          text: this.translate.instant('CONFIRM.BUTTON.OK'),
          handler: () => {
            this.deleteConfirm$.next(true);
          },
        },
      ],
    });
    await alert.present();
  }

  displayNewRepModal(): void {
    this.state.newRep.add = true;
    this.expanded = true;
    this.openRepModal();
    this.state.newRep.exerciceId = this.exercise!.exerciseId;
  }

  updateRepModal(repId: number) {
    this.openRepModal(repId);
  }

  sendExpandedValue(value: boolean) {
    console.log('sendOpenedValue', value);
    this.expanded = value;
    this.sendExpanded.emit(this.expanded);
  }
}
