import { Component, Inject } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-rep',
  templateUrl: './rep.component.html',
  styleUrls: ['./rep.component.scss'],
})
export class RepComponent {
  exerciseTrainingForm: UntypedFormGroup = new UntypedFormGroup({});

  constructor(public dialogRef: MatDialogRef<RepComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.initExerciseTrainingForm();
  }

  validateNewRep(): void {
    const newRep = {
      reps: this.exerciseTrainingForm.get('reps')?.value,
      time: this.exerciseTrainingForm.get('time')?.value,
      weight: this.exerciseTrainingForm.get('weight')?.value,
      distance: this.exerciseTrainingForm.get('distance')?.value,
      kcal: this.exerciseTrainingForm.get('kcal')?.value,
    };
    this.dialogRef.close([newRep, 0]);
  }

  updateRep(): void {
    const newRep = {
      reps: this.exerciseTrainingForm.get('reps')?.value,
      time: this.exerciseTrainingForm.get('time')?.value,
      weight: this.exerciseTrainingForm.get('weight')?.value,
      distance: this.exerciseTrainingForm.get('distance')?.value,
      kcal: this.exerciseTrainingForm.get('kcal')?.value,
    };
    this.dialogRef.close([newRep, 1]);
  }

  deleteRep(): void {
    this.dialogRef.close([null, 2]);
  }

  initExerciseTrainingForm() {
    this.exerciseTrainingForm = new UntypedFormGroup({
      reps: new UntypedFormControl(this.data.rep ? this.data.rep.reps : ''),
      weight: new UntypedFormControl(this.data.rep ? this.data.rep.weight : ''),
      time: new UntypedFormControl(this.data.rep ? this.data.rep.time : ''),
      distance: new UntypedFormControl(this.data.rep ? this.data.rep.distance : ''),
      kcal: new UntypedFormControl(this.data.rep ? this.data.rep.kcal : ''),
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
