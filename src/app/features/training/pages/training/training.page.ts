import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

import { TrainingExercise, TrainingModel } from '../../../../shared/models/training.model';
import { TrainingService } from '../../../../shared/services/training.service';
import { ExerciseService } from '../../../../shared/services/exercise.service';
import { ExerciseModel } from '../../../../shared/models/exercise.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

enum TypeOfUpdate {
  NAME = 'name',
  DATE = 'date',
  TIME = 'time',
  WEIGHT = 'weight',
  PCT_MUSCLE = 'pct_muscle',
  PCT_FAT = 'pct_fat',
}

@Component({
  selector: 'app-training',
  templateUrl: 'training.page.html',
  styleUrls: ['training.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingPage implements OnInit {
  trainingId = '';
  trainingForm: UntypedFormGroup;
  trainingData?: TrainingModel;
  typeOfUpdate = TypeOfUpdate;

  state = {
    loading: false,
  };

  currentExercise = '';
  exerciseList$: Observable<ExerciseModel[]> = this.exerciseService.getExerciseList$;
  debouncer: Subject<any> = new Subject<any>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private trainingService: TrainingService,
    private exerciseService: ExerciseService,
    private ref: ChangeDetectorRef,
  ) {
    this.trainingForm = new UntypedFormGroup({
      title: new UntypedFormControl(''),
      date: new UntypedFormControl(''),
      time: new UntypedFormControl(''),
      exercise: new UntypedFormControl(''),
      weight: new UntypedFormControl(''),
      pct_muscle: new UntypedFormControl(''),
      pct_fat: new UntypedFormControl(''),
    });

    this.debouncer.pipe(debounceTime(1000)).subscribe((value: { type: TypeOfUpdate; val: number }) => {
      this.updateTraining(value.type, value.val);
    });
  }

  ngOnInit() {
    this.trainingId = this.activatedRoute.snapshot.params['trainingId'];
    this.trainingService.getTrainingById(this.trainingId).subscribe((training: TrainingModel) => {
      if (training) {
        this.trainingData = { ...training };
        this.ref.markForCheck();
        this.trainingForm.patchValue({
          title: training.title,
          date: training.creationDate,
          weight: training.weigth,
          pct_muscle: training.pct_muscle,
          pct_fat: training.pct_fat,
        });
      }
    });
    this.trainingForm.get('exercise')?.valueChanges.subscribe(exercise => {
      if (exercise) {
        this.addExercise(exercise);
        this.trainingForm.get('exercise')?.reset();
        this.trainingForm.get('exercise')?.markAsUntouched();
      }
    });
  }

  updateTraining(paramType: TypeOfUpdate, value: Date | string | number): void {
    if (this.trainingData && value) {
      let valueChanged = true;
      const tmpTraining: TrainingModel = { ...this.trainingData };
      switch (paramType) {
        case TypeOfUpdate.DATE:
          tmpTraining.creationDate = value as Date;
          break;
        case TypeOfUpdate.PCT_FAT:
          tmpTraining.pct_fat = +value;
          break;
        case TypeOfUpdate.PCT_MUSCLE:
          tmpTraining.pct_muscle = +value;
          break;
        case TypeOfUpdate.WEIGHT:
          tmpTraining.weigth = +value;
          break;
        case TypeOfUpdate.NAME:
          valueChanged = tmpTraining.title !== value.toString();
          tmpTraining.title = value.toString();
          break;
      }

      if (valueChanged) this.trainingService.updateTraining(tmpTraining);
    }
  }

  addExercise(exerciseId: string): void {
    this.trainingService.addExerciseToTraining(this.trainingId, exerciseId);
  }

  isAlreadyInTraining(exerciseId: string): boolean {
    return (this.trainingData?.exercises || []).some(exercise => exercise.exerciseId === exerciseId) || false;
  }

  deleteExercise(exerciseId: string): void {
    this.trainingService.deleteExerciseToTraining(this.trainingId, exerciseId);
  }

  validateNewRep(exercise: TrainingExercise): void {
    this.trainingService.updateExerciseTraining(this.trainingId, exercise);
  }

  updateOrder() {
    if (this.trainingData?.exercises) {
      for (let i = 0; i <= this.trainingData.exercises.length - 1; i++) {
        this.trainingData.exercises[i].exerciseOrder = i + 1;
      }
      this.trainingService.updateTraining(this.trainingData);
    }
  }

  updateRep(exercise: TrainingExercise): void {
    this.trainingData?.exercises?.forEach(ex => {
      if (ex.exerciseId === exercise.exerciseId) {
        ex = exercise;
      }
    });
    if (this.trainingData) {
      this.trainingService.updateTraining(this.trainingData);
    }
  }

  updateSelectedExercise(expanded: boolean, exercise: TrainingExercise) {
    this.currentExercise = exercise.exerciseId;
  }

  exerciseDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.trainingData!.exercises!, event.previousIndex, event.currentIndex);
    this.updateOrder();
  }
}
