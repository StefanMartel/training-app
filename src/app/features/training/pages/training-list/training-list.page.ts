import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { TrainingModel } from 'src/app/shared/models/training.model';
import { TrainingService } from 'src/app/shared/services/training.service';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-training-list',
  templateUrl: 'training-list.page.html',
  styleUrls: ['training-list.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingListPage {
  deleteConfirm$ = new Subject();

  newTrainingCreating = false;
  newTrainingValue: string;
  isLoading: Observable<boolean>;
  trainingList$: Observable<TrainingModel[]> = this.trainingService.getTrainingList$.pipe(
    map(trainings => trainings.sort((a, b) => new Date(b.creationDate).getTime() - new Date(a.creationDate).getTime())),
  );

  constructor(
    private trainingService: TrainingService,
    private alertController: AlertController,
    private translate: TranslateService,
    private router: Router,
  ) {
    this.newTrainingValue = '';
    this.isLoading = this.trainingService.isLoading$;
  }

  openAddTraining(): void {
    this.newTrainingCreating = true;
  }

  closeAddTraining(): void {
    this.newTrainingCreating = false;
  }

  addTraining(): void {
    if (this.newTrainingValue !== '') {
      this.trainingService.addTraining(this.newTrainingValue);
      this.clearNewValue();
      this.closeAddTraining();
    }
  }

  clearNewValue(): void {
    this.newTrainingValue = '';
  }

  deleteTraining(trainingId: string): void {
    this.deleteConfirm$.next(false);
    this.openDialog();
    this.deleteConfirm$.subscribe(data => {
      if (data) {
        this.trainingService.deleteTraining(trainingId);
      }
    });
  }

  duplicateTraining(training: TrainingModel): void {
    this.trainingService.duplicateTraining(training);
  }

  async openDialog() {
    const alert = await this.alertController.create({
      header: '',
      message: this.translate.instant('CONFIRM.DELETE_TRAINING'),
      buttons: [
        {
          text: this.translate.instant('CONFIRM.BUTTON.CANCEL'),
        },
        {
          text: this.translate.instant('CONFIRM.BUTTON.OK'),
          handler: () => {
            this.deleteConfirm$.next(true);
          },
        },
      ],
    });
    await alert.present();
  }

  goToTraining(trainingId: string): void {
    this.router.navigate(['trainings', trainingId]);
  }
}
