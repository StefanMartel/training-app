import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrainingPage } from './pages/training/training.page';
import { TrainingService } from 'src/app/shared/services/training.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { TrainingListPage } from './pages/training-list/training-list.page';
import { TrainingByIdResolver, TrainingResolver } from './training-resolver.service';
import { TrainingExerciseComponent } from './components/training-exercise/training-exercise.component';
import { RepComponent } from './components/rep/rep.component';
import {IsSignedInGuard} from '../user/user-guard';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
          { path: '', pathMatch: 'full', component: TrainingListPage, canActivate: [IsSignedInGuard] },
          { path: ':trainingId', pathMatch: 'full', component: TrainingPage, resolve: { training: TrainingByIdResolver } },
    ]),
  ],
  declarations: [TrainingPage, TrainingListPage, TrainingExerciseComponent, RepComponent],
  providers: [TrainingService, TrainingResolver, TrainingByIdResolver],
})
export class TrainingModule {}
