import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { StorageService, StorageType } from '../../shared/services/storage.service';

@Injectable({
  providedIn: 'root',
})
export class IsSignedInGuard {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return StorageService.getObject(StorageType.USER).then(value => {
      if (value) {
        return true;
      } else {
        this.router.navigate(['/user']);
        return false;
      }
    });
  }
}
