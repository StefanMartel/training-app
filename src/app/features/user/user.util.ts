import {UntypedFormControl} from "@angular/forms";

export const MAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
export const PASS_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]{8,63}/;

export class UserUtils {

  static isValidEmail(value: string) {
    value = value.trim();
    const isNotEmpty = value ? value.length > 0 : false;
    const isEmail = MAIL_REGEX.test(value);
    return (isNotEmpty && isEmail)
  }

  static isvalidPassword(value: string) {
    return PASS_REGEX.test(value.trim());
  }

  static validateEmail(c: UntypedFormControl) {
    if (c.value && UserUtils.isValidEmail(c.value)) {
      return null;
    } else {
      return {
        email: false,
        empty: false,
      };
    }
  }

}
