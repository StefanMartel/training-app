import { Component} from '@angular/core';
import {UserService} from '../../../../shared/services/user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutPage {

  constructor(private userService: UserService) {
    this.userService.disconnectUser()
  }

}
