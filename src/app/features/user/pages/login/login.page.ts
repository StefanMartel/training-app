import { Component, Injector } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../../../shared/services/user.service';
import { UserUtils } from '../../user.util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  login = new UntypedFormControl('', [Validators.required, UserUtils.validateEmail]);
  password = new UntypedFormControl('', [Validators.required]);
  passwordType: 'password' | 'text' = 'password';

  invalidCredential = false;
  displayErrors = true;
  isPassVisible = false;
  createAccountMode = false;
  canConnectWithFingerprint = false;

  private userService: UserService;

  constructor(public router: Router, injector: Injector) {
    this.userService = injector.get(UserService);
  }

  connectWithFingerPrint() {
    /*this.faio
      .isAvailable()
      .then((result: any) => {
        console.log(result);

        this.faio
          .show({
            cancelButtonTitle: 'Cancel',
            description: 'Some biometric description',
            disableBackup: true,
            title: 'Scanner Title',
            fallbackButtonTitle: 'FB Back Button',
            subtitle: 'This SubTitle',
          })
          .then((result: any) => {
            console.log(result);
            alert('Successfully Authenticated!');
          })
          .catch((error: any) => {
            console.log(error);
            alert('Match not found!');
          });
      })
      .catch((error: any) => {
        console.log(error);
      });*/
  }

  ionViewWillEnter() {
    this.login.reset();
  }

  emptyInput() {
    this.login.reset();
    this.invalidCredential = false;
  }

  connectUser() {
    this.userService.connectUser(this.login.value.trim(), this.password.value.trim());
  }

  openRegistrationPage() {
    this.router.navigate(['user/subscribe']);
  }

  togglePassVisible() {
    this.isPassVisible = !this.isPassVisible;
    this.isPassVisible ? (this.passwordType = 'text') : (this.passwordType = 'password');
  }
}
