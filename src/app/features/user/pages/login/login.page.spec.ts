import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';

// Ionic
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicModule, LoadingController, ModalController } from '@ionic/angular';
import { Components } from '@ionic/core/dist/types/components.d';

// RxJS
import { of } from 'rxjs';

// ngx-translate
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateLoaderMock } from 'src/app/common/services/angular-ionic-mock/translate.mock';

import { RouteMock } from 'src/app/common/services/angular-ionic-mock/route.mock';

// Services
import { PersistentAuthService } from 'src/app/common/services/auth/services/persistent-auth.service';
import { TrackingService } from 'src/app/common/services/tracking/tracking.service';

// Tokens
import { BLUETOOTH_PAGE_URL, ENABLE_BLUETOOTH_BUTTON, REGISTRATION_PAGE_URL } from 'src/environments/tokens';

// Models
import { AuthenticationStatus } from 'src/app/common/services/auth/models/auth-types.model';

// Component under test
import { AccountType, LoginPage, MAIL_REGEX } from './login.page';

// Components
import { PopInComponent } from 'src/app/common/modals/pop-in/pop-in.component';

// Services
import { AuthDataManagementService } from 'src/app/common/services/auth/services/auth-data-management.service';
import { LoggerService } from 'src/app/common/services/logger/logger.service';
import { NetworkService } from 'src/app/common/services/network/network.service';
import { RoutingService } from 'src/app/common/services/routing/routing.service';

// Spies
import { routingServiceSpy } from 'src/app/common/services/angular-ionic-mock/jasmine.mock.spec';

@NgModule({
    imports: [CommonModule, IonicModule],
    declarations: [PopInComponent]
})
class TestModule {}

// eslint-disable-next-line
describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let trackingService: TrackingService;
  let routerService: Router;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  let modalSpy: jasmine.SpyObj<Components.IonModal>;

  const testMail = 'toto@tt.tt';
  const persistentAuthServiceMock = jasmine.createSpyObj('PersistentAuthService', ['startLoggedAuthentification', 'getLoginStatus']);

  const authDataManagementServiceMock = jasmine.createSpyObj('AuthDataManagementService', ['setAuthenticationStatus']);
  const trackingServiceMock = jasmine.createSpyObj('TrackingService', ['trackScreen', 'sendCTAEvent']);
  const loadingSpy = jasmine.createSpyObj('Loading', ['create', 'present', 'dismiss']);
  const loadingControllerSpy = jasmine.createSpyObj('LoadingController', ['create']);
  const networkServiceMock = jasmine.createSpyObj('NetworkService', ['']);
  const loggerServiceMock = jasmine.createSpyObj('LoggerService', ['info']);

  loadingControllerSpy.create.and.callFake(() => {
    return loadingSpy;
  });
  const inAppBrowserMock = jasmine.createSpyObj('InAppBrowser', ['']);

  beforeEach(async(() => {
    modalControllerSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalControllerSpy.create.and.callFake(() => {
      return modalSpy;
    });
    modalSpy = jasmine.createSpyObj('Modal', ['present']);

    TestBed.configureTestingModule({
      declarations: [LoginPage],
      imports: [
        TestModule,
        IonicModule.forRoot(),
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: TranslateLoaderMock },
        }),
      ],
      providers: [
        { provide: NetworkService, useValue: networkServiceMock },
        { provide: LoggerService, useValue: loggerServiceMock },
        { provide: PersistentAuthService, useValue: persistentAuthServiceMock },
        { provide: TrackingService, useValue: trackingServiceMock },
        { provide: Router, useClass: RouteMock },
        { provide: 'IBrowserService', useValue: inAppBrowserMock },
        { provide: BLUETOOTH_PAGE_URL },
        { provide: ENABLE_BLUETOOTH_BUTTON },
        { provide: REGISTRATION_PAGE_URL },
        { provide: ModalController, useValue: modalControllerSpy },
        { provide: LoadingController, useValue: loadingControllerSpy },
        { provide: AuthDataManagementService, useValue: authDataManagementServiceMock },
        { provide: RoutingService, useValue: routingServiceSpy },
        InAppBrowser,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    component.invalidCredential = false;
    fixture.detectChanges();
    trackingService = TestBed.get(TrackingService);
    routerService = TestBed.get(Router);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('openAuthenticatedOpenIdConnect', () => {
    it('should call the tracking function with the paramater /authentification/', () => {
      component.ionViewDidEnter();
      expect(trackingService.trackScreen).toHaveBeenCalledWith('/authentification/');
    });

    describe('login status is AccountType.Telepeage', () => {
      beforeEach(fakeAsync(() => {
        component.email.setValue(testMail);
        persistentAuthServiceMock.getLoginStatus.and.returnValue(
          of({
            Accounts: [
              {
                SubscriberTypeEnum: AccountType.Telepeage,
              },
            ],
          }),
        );
        authDataManagementServiceMock.setAuthenticationStatus.and.returnValue(Promise.resolve(''));
        component.openAuthenticatedOpenIdConnect();
        tick();
      }));

      it('should force the status to not_connected', () => {
        expect(authDataManagementServiceMock.setAuthenticationStatus).toHaveBeenCalledWith(AuthenticationStatus.NOT_CONNECTED);
      });

      it('should call the auth service with name', () => {
        expect(persistentAuthServiceMock.startLoggedAuthentification).toHaveBeenCalledWith(component.email.value);
      });

      it('should call the tracking service with the value "this.trackScreenName, identifiant-tlp-ok"', () => {
        expect(trackingService.sendCTAEvent).toHaveBeenCalledWith(component.trackScreenName, 'identifiant-tlp-ok');
      });
    });

    describe('login status is AccountType.BCU', () => {
      beforeEach(fakeAsync(() => {
        component.email.setValue(testMail);
        persistentAuthServiceMock.getLoginStatus.and.returnValue(
          of({
            Accounts: [
              {
                SubscriberTypeEnum: AccountType.BCU,
              },
            ],
          }),
        );
        spyOn(routerService, 'navigate');
        component.openAuthenticatedOpenIdConnect();
      }));

      it('should call the tracking service with the value "this.trackScreenName, identifiant-bcu"', () => {
        expect(trackingService.sendCTAEvent).toHaveBeenCalledWith(component.trackScreenName, 'identifiant-bcu');
      });

      it('should redirect to "user/information"', () => {
        expect(routerService.navigate).toHaveBeenCalledWith(['user', 'information']);
      });
    });

    describe('login status is AccountType.Free', () => {
      beforeEach(fakeAsync(() => {
        component.email.setValue(testMail);
        persistentAuthServiceMock.getLoginStatus.and.returnValue(
          of({
            Accounts: [
              {
                SubscriberTypeEnum: AccountType.Free,
              },
            ],
          }),
        );
        spyOn(routerService, 'navigate');
        component.openAuthenticatedOpenIdConnect();
        tick();
      }));

      it('should call the tracking service with the value "this.trackScreenName, identifiant-ulys-free"', () => {
        expect(trackingService.sendCTAEvent).toHaveBeenCalledWith(component.trackScreenName, 'identifiant-ulys-free');
      });

      it('should redirect to "user/information"', () => {
        expect(routerService.navigate).toHaveBeenCalledWith(['user', 'information']);
      });
    });

    describe('login status is AccountType.None', () => {
      beforeEach(fakeAsync(() => {
        component.email.setValue(testMail);
        persistentAuthServiceMock.getLoginStatus.and.returnValue(
          of({
            Accounts: [
              {
                SubscriberTypeEnum: AccountType.None,
              },
            ],
          }),
        );
        component.openAuthenticatedOpenIdConnect();
        tick();
      }));

      it('should call the tracking service with the value "this.trackScreenName, identifiant-error"', () => {
        expect(trackingService.sendCTAEvent).toHaveBeenCalledWith(component.trackScreenName, 'identifiant-error');
      });

      it('put the invalidCredential var to true', () => {
        expect(component.invalidCredential).toBe(true);
      });
    });
  });

  describe('validateEmail', () => {
    it('name should respect REGEX pattern', () => {
      component.email.setValue(testMail);
      expect(component.email.value).toMatch(MAIL_REGEX);
    });

    it('name should respect REGEXNUMBER pattern', () => {
      component.email.setValue(2345634);
      expect(component.email.value).not.toBeNaN();
    });
  });

  describe('openHelp', () => {
    it('should call the tracking CTA with the good parameters "this.trackScreenName" && "authentification-information-identifiant" ', fakeAsync(() => {
      component.openHelp();
      tick();
      expect(trackingService.sendCTAEvent).toHaveBeenCalledWith(component.trackScreenName, 'authentification-information-identifiant');
    }));

    it('should call modal with the good parameters "title" and "message" ', fakeAsync(() => {
      component.openHelp();

      tick();

      expect(modalControllerSpy.create).toHaveBeenCalledWith({
        component: jasmine.any(Function),
        cssClass: 'pop-in-modal',
        componentProps: {
          title: 'pages.user-login.pop-in.help.title',
          message: 'pages.user-login.pop-in.help.text',
          icon: '',
        },
      });
    }));
  });
});
