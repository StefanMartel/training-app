import {Component, Injector} from '@angular/core';
import {UntypedFormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {UserUtils} from "../../user.util";

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.page.html',
  styleUrls: ['./subscribe.page.scss'],
})
export class SubscribePage {
  email = new UntypedFormControl('');
  login = new UntypedFormControl('', [Validators.required, UserUtils.validateEmail]);
  password = new UntypedFormControl('', [Validators.required]);
  repeat_password = new UntypedFormControl('');

  invalidCredential = false;
  displayErrors = false;
  isPassVisible = false;

  constructor(public router: Router, public injector: Injector) {

  }

  ionViewWillEnter() {
    this.email.reset();
  }

  createAccount() {

  }

  passEquals() {
    return this.password.value && this.password.value === this.repeat_password.value;
  }

  togglePassVisible() {
    this.isPassVisible = !this.isPassVisible;
  }
}
