import { Component } from '@angular/core';
import { UserService } from '../../../../../shared/services/user.service';

@Component({
    selector: 'app-code-verification',
    templateUrl: './code-verification.page.html',
    styleUrls: ['./code-verification.page.scss'],
})
export class CodeVerificationPage {

    code = '';
    
    constructor( private userService: UserService) {
    }

    validateCode() {
        this.userService.validateCode(this.code);
    }
}
