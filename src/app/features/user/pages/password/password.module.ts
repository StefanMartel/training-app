import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LostPasswordPage } from './lost-password/lost-password.page';
import { SharedModule } from '../../../../shared/shared.module';
import { CodeVerificationPage } from './code-verification/code-verification.page';
import { NewPasswordPage } from './new-password/new-password.page';


const routes: Routes = [
  { path: 'lost', component: LostPasswordPage },
  { path: 'code-verification', component: CodeVerificationPage },
  { path: 'change', component: NewPasswordPage },
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [LostPasswordPage, CodeVerificationPage, NewPasswordPage],
})
export class PasswordModule {
}
