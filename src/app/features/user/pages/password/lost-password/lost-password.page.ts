import { Component } from '@angular/core';
import { UserService } from '../../../../../shared/services/user.service';
import { UserUtils } from '../../../user.util';

@Component({
    selector: 'app-lost-password',
    templateUrl: './lost-password.page.html',
    styleUrls: ['./lost-password.page.scss'],
})
export class LostPasswordPage {

    email = '';
    
    constructor( private userService: UserService) {
    }

    resetPassword() {
        this.userService.resetPassword(this.email.trim());
    }

    isEmail(email: string){
        return UserUtils.isValidEmail(email)
    }

}
