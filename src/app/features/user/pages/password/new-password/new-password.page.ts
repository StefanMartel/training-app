import { Component, Injector } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../../../../shared/services/user.service';
import { UserUtils } from '../../../user.util';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.page.html',
  styleUrls: ['./new-password.page.scss'],
})
export class NewPasswordPage {
  password = new UntypedFormControl('', [Validators.required, this.validatePassword]);
  newPassword = new UntypedFormControl('', [Validators.required, this.validatePassword]);

  displayErrors = true;
  isPassVisible = false;
  createAccountMode = false;
  passwordType: 'password' | 'text' = 'password';

  private userService: UserService;

  constructor(public router: Router, injector: Injector) {
    this.userService = injector.get(UserService);
  }

  changePassword() {
    this.userService.changePassword(this.password.value);
  }

  validatePassword(c: UntypedFormControl) {
    if (UserUtils.isvalidPassword(c.value)) {
      return null;
    } else {
      return {
        password: false,
      };
    }
  }

  togglePassVisible() {
    this.isPassVisible = !this.isPassVisible;
    this.isPassVisible ? (this.passwordType = 'text') : (this.passwordType = 'password');
  }
}
