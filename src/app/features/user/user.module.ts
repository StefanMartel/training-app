import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginPage } from './pages/login/login.page';
import { SharedModule } from '../../shared/shared.module';
import { SubscribePage } from './pages/subscribe/subscribe.page';
import { LogoutPage } from './pages/logout/logout.component';

const routes: Routes = [
  { path: '', component: LoginPage },
  { path: 'subscribe', component: SubscribePage },
  { path: 'logout', component: LogoutPage },
  {
    path: 'password',
    loadChildren: () => import('./pages/password/password.module').then(m => m.PasswordModule),
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [LoginPage, SubscribePage, LogoutPage]
})
export class UserModule {
}
