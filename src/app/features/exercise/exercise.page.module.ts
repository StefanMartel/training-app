import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { ExerciseListPage } from './pages/exercise-list/exercise-list.page';
import { ExercisePage } from './pages/exercise/exercise.page';
import { ExerciseService } from '../../shared/services/exercise.service';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
          { path: '', pathMatch: 'full', component: ExerciseListPage },
          { path: ':exerciseId', pathMatch: 'full', component: ExercisePage },
    ]),
  ],
  declarations: [ExerciseListPage, ExercisePage],
  providers: [ExerciseService],
})
export class ExercisePageModule {}
