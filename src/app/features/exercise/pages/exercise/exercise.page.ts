import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { ExerciseService } from '../../../../shared/services/exercise.service';
import { Subject} from 'rxjs';
import { ExerciseModel } from '../../../../shared/models/exercise.model';
import {debounceTime} from 'rxjs/operators';

enum TypeOfUpdate {
  NAME = 'name',
  DATE = 'date',
  TIME = 'time',
  WEIGHT = 'weight',
  PCT_MUSCLE = 'pct_muscle',
  PCT_FAT = 'pct_fat'
}

@Component({
  selector: 'app-exercise',
  templateUrl: 'exercise.page.html',
  styleUrls: ['exercise.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExercisePage implements OnInit {
  exerciseId: string = '';
  exerciseForm: UntypedFormGroup;
  exerciseData?: ExerciseModel;
  typeOfUpdate = TypeOfUpdate;

  state = {
    loading: false
  };

  debouncer: Subject<any> = new Subject<any>();

  constructor(private activatedRoute: ActivatedRoute, private exerciseService: ExerciseService) {
    this.exerciseForm = new UntypedFormGroup({
      title: new UntypedFormControl(''),
    });


    this.debouncer.pipe(debounceTime(1000)).subscribe( (value: {type: TypeOfUpdate, val: number}) => {
      this.updateExercise(value.type, value.val);
    })
  }

  ngOnInit() {
    this.exerciseId = this.activatedRoute.snapshot.params['exerciseId'];
    this.exerciseService.getExerciseById(this.exerciseId).subscribe((exercise: ExerciseModel) => {
      this.exerciseData = exercise;
      this.exerciseForm.patchValue({
        title: exercise.title,
      });
    });
  }

  updateExercise(paramType: TypeOfUpdate, value: Date | string | number): void{
    if(this.exerciseData){
      let valueChanged = true;
      const tmpExercise: ExerciseModel = {...this.exerciseData};
      switch (paramType){
        case TypeOfUpdate.NAME:
          valueChanged = tmpExercise.title !== value.toString();
          tmpExercise.title = value.toString();
          break;
      }
     if(valueChanged) this.exerciseService.updateExercise(tmpExercise);
    }
  }
}
