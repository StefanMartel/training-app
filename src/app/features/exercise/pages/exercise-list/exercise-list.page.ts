import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { ExerciseService } from '../../../../shared/services/exercise.service';
import { ExerciseModel } from '../../../../shared/models/exercise.model';

@Component({
  selector: 'app-exercise-list',
  templateUrl: 'exercise-list.page.html',
  styleUrls: ['exercise-list.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExerciseListPage {
  deleteConfirm$ = new Subject();

  newExerciseCreating = false;
  newExerciseValue: string;

  exerciseList$: Observable<ExerciseModel[]> = this.exerciseService.getExerciseList$;

  constructor(
    private exerciseService: ExerciseService,
    private alertController: AlertController,
    private translate: TranslateService,
    private router: Router,
  ) {
    this.newExerciseValue = '';
  }

  openAddExercise() {
    this.newExerciseCreating = true;
  }

  closeAddExercise() {
    this.newExerciseCreating = false;
  }

  addExercise() {
    if (this.newExerciseValue !== '') {
      this.exerciseService.addExercise(this.newExerciseValue);
      this.clearNewValue();
      this.closeAddExercise();
    }
  }

  clearNewValue() {
    this.newExerciseValue = '';
  }

  deleteExercise(exerciseId: string) {
    this.deleteConfirm$.next(false);
    this.openDialog();
    this.deleteConfirm$.subscribe(data => {
      if (data) {
        this.exerciseService.deleteExercise(exerciseId);
      }
    });
  }

  async openDialog() {
    const alert = await this.alertController.create({
      header: '',
      message: this.translate.instant('CONFIRM.DELETE_TRAINING'),
      buttons: [
        {
          text: this.translate.instant('CONFIRM.BUTTON.CANCEL'),
        },
        {
          text: this.translate.instant('CONFIRM.BUTTON.OK'),
          handler: () => {
            this.deleteConfirm$.next(true);
          },
        },
      ],
    });
    await alert.present();
  }

  goToExercise(exerciseId: string) {
    this.router.navigate(['exercises', exerciseId]);
  }
}
