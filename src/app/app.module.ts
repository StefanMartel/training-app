import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { reducers } from './shared/store/reducers/index';
import { TrainingEffects } from './shared/store/training/training.effects';
import { HttpRepo } from './shared/http/http';
import { ErrorInterceptor } from './shared/interceptors/error.interceptor';
import { ExerciseEffects } from './shared/store/exercise/exercise.effects';
import { TrainingService } from './shared/services/training.service';
import { ExerciseService } from './shared/services/exercise.service';
import { UrlConfiguration } from './shared/http/configuration/url.configuration';
import { LoadingService } from './shared/services/loading.service';
import { UserEffects } from './shared/store/user/user.effects';
import { UserService } from './shared/services/user.service';
import { StorageService } from './shared/services/storage.service';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function httpRepo(http: HttpClient) {
  return new HttpRepo(http);
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    StoreModule.forRoot({ reducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 10,
    }),
    EffectsModule.forRoot([TrainingEffects, ExerciseEffects, UserEffects]),
    AppRoutingModule,
    SharedModule,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HttpRepo, useFactory: httpRepo, deps: [HttpClient] },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    TrainingService,
    ExerciseService,
    UserService,
    UrlConfiguration,
    LoadingService,
    StorageService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
