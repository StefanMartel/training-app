import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Store } from '@ngrx/store';

import { StatusBar, Style } from '@capacitor/status-bar';
import { SplashScreen } from '@capacitor/splash-screen';
import { SetUserAction } from './shared/store/user/user.action';
import { GetTrainingListAction } from './shared/store/training/training.action';
import { GetExerciseListAction } from './shared/store/exercise/exercise.action';
import { LoadingService } from './shared/services/loading.service';
import { Observable } from 'rxjs';
import { StorageService, StorageType } from './shared/services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  constructor(private platform: Platform, private store: Store<any>, private loadingService: LoadingService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.setStyle({ style: Style.Dark });
      StorageService.getObject(StorageType.USER)
        .then(user => {
          if (user) {
            this.store.dispatch(new SetUserAction(user));
            this.store.dispatch(new GetTrainingListAction(user.login));
            this.store.dispatch(new GetExerciseListAction());
          }
        })
        .finally(() => SplashScreen.hide());
    });
  }

  isLoading(): Observable<boolean> {
    return this.loadingService.isLoading();
  }
}
